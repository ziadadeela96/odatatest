﻿using R365.WebApi.Areas.v1.Views.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Linq.Dynamic;

namespace R365.WebApi.Controllers
{
    public class ACompanyController : ApiController
    {

        public ACompanyController()
            : base()
        {

        }

        ApiV1Context context = new ApiV1Context();

        // GET api/company
        public IEnumerable<company> GetCompanies()
        {
            return context.company.ToArray();
        }

        [Route("api/acompany/topn/{n}")]
        [HttpGet]
        public IEnumerable<company> GetTopNCompanies(int n)
        {
            return context.company.Take(n).ToArray();
        }

        [Route("api/acompany/count")]
        [HttpGet]
        public int GetCompaniesCount()
        {
            return context.company.Count();
        }

        [Route("api/acompany/order")]
        [HttpGet]
        public IEnumerable<company> GetOrderedCompanies(string column="accountId",string dir="ascending")
        {
            //Dynamic linq was used to impelement dynamic ordering
            return context.company.OrderBy($"{column} {dir}").ToArray();

        }

        [Route("api/acompany/select")]
        [HttpGet]
        public IQueryable GetSelectedAttributesFromCompanies(string select = "")
        {
            //Dynamic linq was used to impelement dynamic ordering
            return context.company.Select($"new({select})");

        }

        [Route("api/acompany/filter")]
        [HttpGet]
        public IEnumerable<company> GetFilteredCompanies(string name = null, string companyNumber = null, string comment = null)
        {

            return context.company.Where(c=> 
            (String.IsNullOrEmpty(name) || c.companyName.Contains(name))&&
            (String.IsNullOrEmpty(companyNumber) || c.companyNumber.Contains(companyNumber))&&
            (String.IsNullOrEmpty(comment) || c.comment.Contains(comment))
            );
        }





        public company GetCompany(Guid id)
        {
            var matchedCompany = context.company.FirstOrDefault(c => c.accountId == id);
            if (matchedCompany == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
            return matchedCompany;
        }

        

        public void PostCompany(company company)
        {
            context.company.Add(company);
            context.SaveChanges();
        }

        public company PutCompany(Guid id,company company)
        {
            var matchedCompany = context.company.FirstOrDefault(c => c.accountId == id);
            if (matchedCompany == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
            return company;
        }

        public void DeleteCompany(Guid id)
        {
            var matchedCompany = context.company.FirstOrDefault(c => c.accountId == id);
            if (matchedCompany == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
            context.company.Remove(matchedCompany);
            context.SaveChanges();

        }
    }
}