﻿using R365.WebApi.Areas.v1.Views.EF;
using R365.WebApi.Authentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.OData;

namespace R365.WebApi.Controllers
{

    //[Authorize]
    //[IdentityBasicAuthentication]
    public class ValuesController : ApiController
    {
        ApiV1Context context = new ApiV1Context();

        // GET api/values
        [EnableQuery]
        public IQueryable<company> Get()
        {
            return context.company;
        }

        //// GET api/values/5
        //public string Get(int id)
        //{
        //    return "value";
        //}

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
