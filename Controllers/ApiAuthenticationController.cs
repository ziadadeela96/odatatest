﻿using R365.WebApi.Authentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Results;

namespace R365.WebApi.Controllers
{
    [Authorize]
    [IdentityBasicAuthentication]
    public class ApiAuthenticationController : ApiController
    {
        public AuthenticationResult Get()
        {
            return new AuthenticationResult();
        }
    }
    public class AuthenticationResult
    {
        public string Result { get { return "Success"; } }
    }
}