﻿using AutoMapper;
using R365.WebApi.Areas.v1.Views;
using R365.WebApi.Areas.v1.Views.EF;
using R365.WebApi.Areas.v1.Views.Repositories;
using R365.WebApi.Authentication;
using System;
using System.Web;
using System.Web.OData;

namespace R365.WebApi.Controllers
{

    public abstract class ApiBaseController : ODataController
    {
        protected Repositories Repos => _repos.Value;
        private Lazy<Repositories> _repos;
        private Lazy<ApiV1Context> _context;
        private IMapper _mapper;

        public ApiBaseController()
        {
            this._context = new Lazy<ApiV1Context>();

            this._repos = new Lazy<Repositories>(() => new Repositories(this._context.Value));
            this._mapper = AutoMapperConfig.GetMapperConfiguration().CreateMapper();
        }
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (this._context.IsValueCreated)
                this._context.Value.Dispose();
        }
        protected Guid? CurrentUserId
        {
            get { return ((UserIdentity)HttpContext.Current?.User?.Identity).UserId; }
        }
        protected UserIdentity CurrentUser
        {
            get => (UserIdentity)HttpContext.Current?.User?.Identity;
        }
    }
}