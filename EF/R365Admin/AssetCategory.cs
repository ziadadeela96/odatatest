namespace R365.WebApi.EF.R365Admin
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AssetCategory")]
    public partial class AssetCategory
    {
        public Guid Id { get; set; }

        [StringLength(100)]
        public string Name { get; set; }

        public Guid? ServiceVendor { get; set; }

        public Guid? ServiceEmployee { get; set; }

        public DateTime? CreatedOn { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public Guid? ModifiedBy { get; set; }

        public Guid? CreatedBy { get; set; }
    }
}
