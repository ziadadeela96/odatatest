namespace R365.WebApi.EF.R365Admin
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class R365UserRoles
    {
        public Guid ID { get; set; }

        public Guid? UserId { get; set; }

        public Guid? RoleId { get; set; }
    }
}
