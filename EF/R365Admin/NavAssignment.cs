namespace R365.WebApi.EF.R365Admin
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("NavAssignment")]
    public partial class NavAssignment
    {
        public Guid NavAssignmentID { get; set; }

        public int? Module { get; set; }

        [StringLength(100)]
        public string SubSection { get; set; }

        [StringLength(100)]
        public string NavLine { get; set; }

        public Guid? NewFeature { get; set; }
    }
}
