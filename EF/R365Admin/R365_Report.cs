namespace R365.WebApi.EF.R365Admin
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class R365_Report
    {
        public Guid Id { get; set; }

        [Required]
        [StringLength(250)]
        public string ReportName { get; set; }

        [Required]
        [StringLength(250)]
        public string ReportPath { get; set; }

        public DateTime ModifiedOn { get; set; }

        [Required]
        [StringLength(50)]
        public string Category { get; set; }
    }
}
