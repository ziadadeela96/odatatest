namespace R365.WebApi.EF.R365Admin
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class R365_DSQueryParameters
    {
        public Guid Id { get; set; }

        [Required]
        [StringLength(250)]
        public string QueryParameterName { get; set; }

        public Guid DataSetId { get; set; }

        [StringLength(50)]
        public string StaticValue { get; set; }

        public Guid? ParameterId { get; set; }

        public DateTime ModifiedOn { get; set; }
    }
}
