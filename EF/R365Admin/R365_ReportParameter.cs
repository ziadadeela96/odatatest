namespace R365.WebApi.EF.R365Admin
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class R365_ReportParameter
    {
        public Guid Id { get; set; }

        [Required]
        [StringLength(100)]
        public string ReportParameterName { get; set; }

        [StringLength(100)]
        public string Prompt { get; set; }

        public Guid ReportId { get; set; }

        [Required]
        [StringLength(50)]
        public string DataType { get; set; }

        public bool Visible { get; set; }

        public bool QuickAdd { get; set; }

        public int Order { get; set; }

        public DateTime ModifiedOn { get; set; }

        [StringLength(250)]
        public string DefaultValue { get; set; }

        public bool? MultiValue { get; set; }
    }
}
