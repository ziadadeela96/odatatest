namespace R365.WebApi.EF.R365Admin
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class R365AdminContext : DbContext
    {
        public R365AdminContext(string userName, string password)
            : base($"Server=10.142.29.94; Database=R365;User Id={userName};Password={password};")
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            this.OnModelCreatingInt(modelBuilder);
        }
    }
}
