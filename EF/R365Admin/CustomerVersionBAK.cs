namespace R365.WebApi.EF.R365Admin
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CustomerVersionBAK")]
    public partial class CustomerVersionBAK
    {
        [StringLength(50)]
        public string CustomerInstance { get; set; }

        [StringLength(250)]
        public string ReportPath { get; set; }

        [StringLength(250)]
        public string AppPath { get; set; }

        [StringLength(250)]
        public string SQLServer { get; set; }

        [Key]
        public int CustomerID { get; set; }

        public int? LocationCount { get; set; }

        public Guid? SalesRep { get; set; }

        public int? LocationCountOpportunity { get; set; }

        [StringLength(50)]
        public string Environment { get; set; }

        public DateTime? LastIndexed { get; set; }

        public int? UTCOffset { get; set; }

        public bool? Escalated { get; set; }
    }
}
