namespace R365.WebApi.EF.R365Admin
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class R365_ReportParameterValidValues
    {
        public Guid Id { get; set; }

        public Guid ParameterId { get; set; }

        [StringLength(50)]
        public string ParameterValue { get; set; }

        [StringLength(50)]
        public string ParameterLabel { get; set; }

        public Guid? DataSetRefId { get; set; }

        [StringLength(50)]
        public string ValueField { get; set; }

        [StringLength(50)]
        public string LabelField { get; set; }
    }
}
