namespace R365.WebApi.EF.R365Admin
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("VendorExternalAPI")]
    public partial class VendorExternalAPI
    {
        [Key]
        public int VenExternalID { get; set; }

        [StringLength(200)]
        public string VenExternalSID { get; set; }

        [StringLength(200)]
        public string VenExternalToken { get; set; }

        public DateTime? CreateDate { get; set; }

        public DateTime? UpdateDate { get; set; }

        public bool? EnableFlag { get; set; }

        [StringLength(200)]
        public string VenContact { get; set; }

        [StringLength(50)]
        public string VenName { get; set; }

        [StringLength(50)]
        public string CustomerInstance { get; set; }
    }
}
