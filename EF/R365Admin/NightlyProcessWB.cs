namespace R365.WebApi.EF.R365Admin
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("NightlyProcessWB")]
    public partial class NightlyProcessWB
    {
        [Key]
        public int npID { get; set; }

        [StringLength(100)]
        public string db { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? dt { get; set; }

        [StringLength(250)]
        public string process { get; set; }

        public string error { get; set; }
    }
}
