namespace R365.WebApi.EF.R365Admin
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("timezonebyzipcode")]
    public partial class timezonebyzipcode
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int idtimezonebyzipcode { get; set; }

        [StringLength(5)]
        public string zip { get; set; }

        [StringLength(45)]
        public string city { get; set; }

        [StringLength(45)]
        public string county { get; set; }

        [StringLength(2)]
        public string state { get; set; }

        [StringLength(45)]
        public string country { get; set; }

        [StringLength(45)]
        public string timezone { get; set; }

        public int? addressquality { get; set; }

        [StringLength(45)]
        public string source { get; set; }

        [Column(TypeName = "date")]
        public DateTime? sourcedate { get; set; }
    }
}
