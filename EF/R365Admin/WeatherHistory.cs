namespace R365.WebApi.EF.R365Admin
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("WeatherHistory")]
    public partial class WeatherHistory
    {
        [Key]
        public int weatherHisoryId { get; set; }

        [StringLength(100)]
        public string zipCode { get; set; }

        public decimal? high { get; set; }

        public decimal? Low { get; set; }

        [Column(TypeName = "date")]
        public DateTime? dt { get; set; }

        [StringLength(100)]
        public string icon { get; set; }

        [StringLength(100)]
        public string weatherDescription { get; set; }
    }
}
