namespace R365.WebApi.EF.R365Admin
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class R365_ReportTransactions
    {
        public Guid Id { get; set; }

        public Guid ReportId { get; set; }

        [Required]
        [StringLength(250)]
        public string ReportName { get; set; }

        [Required]
        [StringLength(50)]
        public string ReportPath { get; set; }

        public int TransactionType { get; set; }

        public bool Processed { get; set; }

        public DateTime ModifiedOn { get; set; }
    }
}
