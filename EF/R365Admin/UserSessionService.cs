namespace R365.WebApi.EF.R365Admin
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("UserSessionService")]
    public partial class UserSessionService
    {
        [Key]
        [Column(Order = 0)]
        public int ID { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(50)]
        public string UserId { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(50)]
        public string FullName { get; set; }

        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int dm_ChangePassword { get; set; }

        [Key]
        [Column(Order = 4)]
        [StringLength(50)]
        public string LoginTime { get; set; }

        [Key]
        [Column(Order = 5)]
        [StringLength(250)]
        public string DefaultPage { get; set; }

        [Key]
        [Column(Order = 6)]
        [StringLength(250)]
        public string dm_R365 { get; set; }

        [Key]
        [Column(Order = 7)]
        [StringLength(100)]
        public string Logo { get; set; }
    }
}
