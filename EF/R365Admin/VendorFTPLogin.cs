namespace R365.WebApi.EF.R365Admin
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("VendorFTPLogin")]
    public partial class VendorFTPLogin
    {
        [Key]
        public int VenFTPID { get; set; }

        [StringLength(30)]
        public string VenFTPuser { get; set; }

        [StringLength(50)]
        public string VenFTPPass { get; set; }

        public DateTime? CreateDate { get; set; }

        public DateTime? UpdateDate { get; set; }

        public bool? EnableFlag { get; set; }

        [StringLength(200)]
        public string EmailAddress { get; set; }
    }
}
