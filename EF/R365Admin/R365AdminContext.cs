namespace R365.WebApi.EF.R365Admin
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class R365AdminContext : DbContext
    {
        public virtual DbSet<AssetCategory> AssetCategory { get; set; }
        public virtual DbSet<FTPVendorFilePrefix> FTPVendorFilePrefix { get; set; }
        public virtual DbSet<NavAssignment> NavAssignment { get; set; }
        public virtual DbSet<NewFeature> NewFeature { get; set; }
        public virtual DbSet<NightlyProcessWB> NightlyProcessWB { get; set; }
        public virtual DbSet<R365_DSQueryParameters> R365_DSQueryParameters { get; set; }
        public virtual DbSet<R365_Report> R365_Report { get; set; }
        public virtual DbSet<R365_ReportDataSet> R365_ReportDataSet { get; set; }
        public virtual DbSet<R365_ReportParameter> R365_ReportParameter { get; set; }
        public virtual DbSet<R365_ReportParameterValidValues> R365_ReportParameterValidValues { get; set; }
        public virtual DbSet<R365_ReportTransactions> R365_ReportTransactions { get; set; }
        public virtual DbSet<R365Roles> R365Roles { get; set; }
        public virtual DbSet<R365User> R365User { get; set; }
        public virtual DbSet<R365UserRoles> R365UserRoles { get; set; }
        public virtual DbSet<timezonebyzipcode> timezonebyzipcode { get; set; }
        public virtual DbSet<UserNewFeature> UserNewFeature { get; set; }
        public virtual DbSet<VendorExternalAPI> VendorExternalAPI { get; set; }
        public virtual DbSet<VendorFTPLogin> VendorFTPLogin { get; set; }
        public virtual DbSet<WeatherHistory> WeatherHistory { get; set; }
        public virtual DbSet<CustomerUsers> CustomerUsers { get; set; }
        public virtual DbSet<CustomerVersion> CustomerVersion { get; set; }
        public virtual DbSet<CustomerVersionBAK> CustomerVersionBAK { get; set; }
        public virtual DbSet<EmailCredential> EmailCredential { get; set; }
        public virtual DbSet<LoginPage> LoginPage { get; set; }
        public virtual DbSet<UserSessionService> UserSessionService { get; set; }

        protected  void OnModelCreatingInt(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<NightlyProcessWB>()
                .Property(e => e.error)
                .IsUnicode(false);

            modelBuilder.Entity<R365_Report>()
                .Property(e => e.Category)
                .IsUnicode(false);

            modelBuilder.Entity<R365_ReportParameter>()
                .Property(e => e.ReportParameterName)
                .IsUnicode(false);

            modelBuilder.Entity<R365_ReportParameter>()
                .Property(e => e.Prompt)
                .IsUnicode(false);

            modelBuilder.Entity<R365_ReportParameter>()
                .Property(e => e.DataType)
                .IsUnicode(false);

            modelBuilder.Entity<R365_ReportParameter>()
                .Property(e => e.DefaultValue)
                .IsUnicode(false);

            modelBuilder.Entity<R365_ReportParameterValidValues>()
                .Property(e => e.ParameterValue)
                .IsUnicode(false);

            modelBuilder.Entity<R365_ReportParameterValidValues>()
                .Property(e => e.ParameterLabel)
                .IsUnicode(false);

            modelBuilder.Entity<R365_ReportParameterValidValues>()
                .Property(e => e.ValueField)
                .IsUnicode(false);

            modelBuilder.Entity<R365_ReportParameterValidValues>()
                .Property(e => e.LabelField)
                .IsUnicode(false);

            modelBuilder.Entity<R365_ReportTransactions>()
                .Property(e => e.ReportName)
                .IsUnicode(false);

            modelBuilder.Entity<R365_ReportTransactions>()
                .Property(e => e.ReportPath)
                .IsUnicode(false);

            modelBuilder.Entity<timezonebyzipcode>()
                .Property(e => e.zip)
                .IsUnicode(false);

            modelBuilder.Entity<timezonebyzipcode>()
                .Property(e => e.city)
                .IsUnicode(false);

            modelBuilder.Entity<timezonebyzipcode>()
                .Property(e => e.county)
                .IsUnicode(false);

            modelBuilder.Entity<timezonebyzipcode>()
                .Property(e => e.state)
                .IsUnicode(false);

            modelBuilder.Entity<timezonebyzipcode>()
                .Property(e => e.country)
                .IsUnicode(false);

            modelBuilder.Entity<timezonebyzipcode>()
                .Property(e => e.timezone)
                .IsUnicode(false);

            modelBuilder.Entity<timezonebyzipcode>()
                .Property(e => e.source)
                .IsUnicode(false);

            modelBuilder.Entity<UserNewFeature>()
                .HasOptional(e => e.UserNewFeature1)
                .WithRequired(e => e.UserNewFeature2);

            modelBuilder.Entity<VendorExternalAPI>()
                .Property(e => e.VenName)
                .IsUnicode(false);

            modelBuilder.Entity<VendorExternalAPI>()
                .Property(e => e.CustomerInstance)
                .IsUnicode(false);

            modelBuilder.Entity<WeatherHistory>()
                .Property(e => e.high)
                .HasPrecision(18, 5);

            modelBuilder.Entity<WeatherHistory>()
                .Property(e => e.Low)
                .HasPrecision(18, 5);

            modelBuilder.Entity<EmailCredential>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<EmailCredential>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<EmailCredential>()
                .Property(e => e.Password)
                .IsUnicode(false);

            modelBuilder.Entity<EmailCredential>()
                .Property(e => e.Server)
                .IsUnicode(false);
        }
    }
}
