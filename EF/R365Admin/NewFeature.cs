namespace R365.WebApi.EF.R365Admin
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("NewFeature")]
    public partial class NewFeature
    {
        public Guid NewFeatureID { get; set; }

        [StringLength(100)]
        public string Title { get; set; }

        public string Description { get; set; }

        public Guid? AcademyCourse { get; set; }

        public Guid? AcademyLesson { get; set; }

        [StringLength(200)]
        public string DocumentationURL { get; set; }

        [StringLength(200)]
        public string DocumentationDescription { get; set; }

        [StringLength(200)]
        public string Video { get; set; }

        public bool? Published { get; set; }

        [Column(TypeName = "date")]
        public DateTime? PublishStartDate { get; set; }

        [Column(TypeName = "date")]
        public DateTime? PublishEndDate { get; set; }
    }
}
