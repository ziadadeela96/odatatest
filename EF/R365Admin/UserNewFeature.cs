namespace R365.WebApi.EF.R365Admin
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("UserNewFeature")]
    public partial class UserNewFeature
    {
        public int Id { get; set; }

        public Guid? NewFeatureId { get; set; }

        public Guid? UserId { get; set; }

        public virtual UserNewFeature UserNewFeature1 { get; set; }

        public virtual UserNewFeature UserNewFeature2 { get; set; }
    }
}
