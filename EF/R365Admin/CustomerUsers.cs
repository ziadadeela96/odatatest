namespace R365.WebApi.EF.R365Admin
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class CustomerUsers
    {
        public int Id { get; set; }

        [StringLength(128)]
        public string dbname { get; set; }

        [StringLength(160)]
        public string dm_login { get; set; }

        public bool? dm_R365 { get; set; }

        public Guid? UserId { get; set; }
    }
}
