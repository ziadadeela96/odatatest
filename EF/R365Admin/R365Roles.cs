namespace R365.WebApi.EF.R365Admin
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class R365Roles
    {
        [Key]
        public Guid RoleId { get; set; }

        [StringLength(100)]
        public string Name { get; set; }

        public bool? IsPrimaryRole { get; set; }
    }
}
