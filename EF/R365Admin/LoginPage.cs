namespace R365.WebApi.EF.R365Admin
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("LoginPage")]
    public partial class LoginPage
    {
        public Guid Id { get; set; }

        [StringLength(200)]
        public string BackgroundImage { get; set; }

        [StringLength(200)]
        public string InfoTitle { get; set; }

        [StringLength(1000)]
        public string InfoText { get; set; }

        [StringLength(200)]
        public string InfoLinkText { get; set; }

        [StringLength(200)]
        public string InfoLinkURL { get; set; }

        [StringLength(200)]
        public string InfoImage { get; set; }

        public DateTime? CreatedOn { get; set; }

        public Guid? CreatedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public Guid? ModifiedBy { get; set; }

        public string TermsAndConditions { get; set; }
    }
}
