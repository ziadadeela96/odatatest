namespace R365.WebApi.EF.R365Admin
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class R365_ReportDataSet
    {
        public Guid Id { get; set; }

        [Required]
        [StringLength(250)]
        public string DataSetName { get; set; }

        public Guid ReportId { get; set; }

        [StringLength(50)]
        public string CommandType { get; set; }

        public string CommandText { get; set; }

        public DateTime ModifiedOn { get; set; }
    }
}
