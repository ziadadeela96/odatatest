namespace R365.WebApi.EF.R365
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class R365Context : DbContext
    {
        public virtual DbSet<Roles> Roles { get; set; }
        public virtual DbSet<SystemUserBase> SystemUserBase { get; set; }
        public virtual DbSet<UserRole> UserRole { get; set; }

        protected void OnModelCreatingInt(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<SystemUserBase>()
                .Property(e => e.VersionNumber)
                .IsFixedLength();

            modelBuilder.Entity<SystemUserBase>()
                .Property(e => e.ExchangeRate)
                .HasPrecision(23, 10);
        }
    }
}
