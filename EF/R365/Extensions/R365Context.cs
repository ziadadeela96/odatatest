namespace R365.WebApi.EF.R365
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Web.Configuration;

    public partial class R365Context : DbContext
    {
        public R365Context(string server, string database, string userName, string password)
            : base($"Server={server};Database={database};User Id={userName};Password={password};")
        {
        }

        public R365Context()
            : base($"Server=localhost;Database={WebConfigurationManager.AppSettings["DbName"]};Integrated Security=SSPI;")
        { }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            this.OnModelCreatingInt(modelBuilder);
        }
    }
}
