namespace R365.WebApi.EF.R365
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SystemUserBase")]
    public partial class SystemUserBase
    {
        [Key]
        public Guid SystemUserId { get; set; }

        public Guid? TerritoryId { get; set; }

        public Guid? OrganizationId { get; set; }

        public Guid? BusinessUnitId { get; set; }

        public Guid? ParentSystemUserId { get; set; }

        [StringLength(50)]
        public string FirstName { get; set; }

        [StringLength(20)]
        public string Salutation { get; set; }

        [StringLength(50)]
        public string MiddleName { get; set; }

        [StringLength(50)]
        public string LastName { get; set; }

        [StringLength(100)]
        public string PersonalEMailAddress { get; set; }

        [StringLength(160)]
        public string FullName { get; set; }

        [StringLength(50)]
        public string NickName { get; set; }

        [StringLength(100)]
        public string Title { get; set; }

        [StringLength(100)]
        public string InternalEMailAddress { get; set; }

        [StringLength(100)]
        public string JobTitle { get; set; }

        [StringLength(100)]
        public string MobileAlertEMail { get; set; }

        public int? PreferredEmailCode { get; set; }

        [StringLength(50)]
        public string HomePhone { get; set; }

        [StringLength(50)]
        public string MobilePhone { get; set; }

        public int? PreferredPhoneCode { get; set; }

        public int? PreferredAddressCode { get; set; }

        [StringLength(200)]
        public string PhotoUrl { get; set; }

        [StringLength(255)]
        public string DomainName { get; set; }

        public int? PassportLo { get; set; }

        public DateTime? CreatedOn { get; set; }

        public int? PassportHi { get; set; }

        [StringLength(500)]
        public string DisabledReason { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public Guid? CreatedBy { get; set; }

        [StringLength(100)]
        public string EmployeeId { get; set; }

        public Guid? ModifiedBy { get; set; }

        public bool? IsDisabled { get; set; }

        [StringLength(100)]
        public string GovernmentId { get; set; }

        [Column(TypeName = "timestamp")]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [MaxLength(8)]
        public byte[] VersionNumber { get; set; }

        [StringLength(100)]
        public string Skills { get; set; }

        public bool? DisplayInServiceViews { get; set; }

        public Guid? CalendarId { get; set; }

        public Guid? ActiveDirectoryGuid { get; set; }

        public bool? SetupUser { get; set; }

        public Guid? SiteId { get; set; }

        [StringLength(100)]
        public string WindowsLiveID { get; set; }

        public int? IncomingEmailDeliveryMethod { get; set; }

        public int? OutgoingEmailDeliveryMethod { get; set; }

        public int? ImportSequenceNumber { get; set; }

        public int? AccessMode { get; set; }

        public int? InviteStatusCode { get; set; }

        public bool? IsActiveDirectoryUser { get; set; }

        public DateTime? OverriddenCreatedOn { get; set; }

        public int? UTCConversionTimeZoneCode { get; set; }

        public int? TimeZoneRuleVersionNumber { get; set; }

        [StringLength(160)]
        public string YomiFullName { get; set; }

        [StringLength(50)]
        public string YomiLastName { get; set; }

        [StringLength(50)]
        public string YomiMiddleName { get; set; }

        [StringLength(50)]
        public string YomiFirstName { get; set; }

        public Guid? CreatedOnBehalfBy { get; set; }

        public decimal? ExchangeRate { get; set; }

        public bool? IsIntegrationUser { get; set; }

        public Guid? ModifiedOnBehalfBy { get; set; }

        public int? EmailRouterAccessApproval { get; set; }

        public bool? DefaultFiltersPopulated { get; set; }

        public int? CALType { get; set; }

        public Guid? QueueId { get; set; }

        public Guid? TransactionCurrencyId { get; set; }

        public bool? IsSyncWithDirectory { get; set; }

        public bool? IsLicensed { get; set; }

        [StringLength(200)]
        public string dm_EmailPassword { get; set; }

        [StringLength(200)]
        public string dm_EmailOutGoingMailServer { get; set; }

        [StringLength(200)]
        public string dm_EmailPort { get; set; }

        public bool? dm_EmailSSL { get; set; }

        [StringLength(200)]
        public string dm_Login { get; set; }

        [StringLength(200)]
        public string dm_LoginPassword { get; set; }

        public bool? dm_ChangePassword { get; set; }

        public bool? dm_AvailableToAllLocations { get; set; }

        public bool? dm_Inactive { get; set; }

        public bool? dm_R365 { get; set; }

        public Guid? dm_Location { get; set; }

        [StringLength(100)]
        public string defaultPage { get; set; }

        [StringLength(250)]
        public string DefaultDashboard { get; set; }

        public int? dm_AllReports { get; set; }

        public bool? dm_NewFeature { get; set; }

        public int? dm_Theme { get; set; }

        public int? R365Manager { get; set; }

        public bool? FullAccess { get; set; }
    }
}
