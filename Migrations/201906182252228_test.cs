namespace R365.WebApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.company",
                c => new
                    {
                        accountId = c.Guid(nullable: false),
                        companyName = c.String(maxLength: 160),
                        companyNumber = c.String(maxLength: 100),
                        comment = c.String(maxLength: 200),
                        modifiedOn = c.DateTime(),
                        modifiedBy = c.Guid(),
                        createdOn = c.DateTime(),
                        createdBy = c.Guid(),
                    })
                .PrimaryKey(t => t.accountId);
            
            CreateTable(
                "dbo.employee",
                c => new
                    {
                        employeeId = c.Guid(nullable: false),
                        fullName = c.String(maxLength: 100),
                        address1 = c.String(maxLength: 100),
                        address2 = c.String(maxLength: 100),
                        allowTextMessaging = c.Boolean(),
                        birthdayDay = c.Int(),
                        birthdayMonth = c.Int(),
                        city = c.String(maxLength: 100),
                        firstName = c.String(maxLength: 100),
                        hireDate = c.DateTime(),
                        lastName = c.String(maxLength: 100),
                        middleName = c.String(maxLength: 100),
                        mobilePhone = c.String(maxLength: 100),
                        multipleLocations = c.Int(),
                        payrollID = c.String(maxLength: 100),
                        phoneNumber = c.String(maxLength: 100),
                        pOSID = c.String(maxLength: 100),
                        state = c.String(maxLength: 100),
                        zipCode = c.String(maxLength: 100),
                        employeeLink = c.Guid(),
                        primaryLocation_id = c.Guid(),
                        createdOn = c.DateTime(),
                        modifiedOn = c.DateTime(),
                        inactive = c.Boolean(),
                        email = c.String(maxLength: 200),
                        birthday = c.DateTime(),
                    })
                .PrimaryKey(t => t.employeeId);
            
            CreateTable(
                "dbo.glAccount",
                c => new
                    {
                        glAccountAutoId = c.Long(nullable: false, identity: true),
                        locationNo = c.String(maxLength: 7, unicode: false),
                        locationName = c.String(maxLength: 60),
                        locationId = c.Guid(),
                        legalEntityNumber = c.String(maxLength: 7, unicode: false),
                        legalEntityName = c.String(maxLength: 60),
                        legalEntityId = c.Guid(),
                        attribute1Number = c.String(maxLength: 7, unicode: false),
                        attribute1Name = c.String(maxLength: 60),
                        attribute1Id = c.Guid(),
                        attribute2Number = c.String(maxLength: 7, unicode: false),
                        attribute2Name = c.String(maxLength: 60),
                        attribute2Id = c.Guid(),
                        glAccountId = c.Guid(),
                        glAccountName = c.String(maxLength: 100),
                        glAccountNumber = c.String(maxLength: 13, unicode: false),
                        glAccount_Number = c.String(maxLength: 100),
                        glTypeClass = c.Int(),
                        glType = c.String(maxLength: 60),
                        operationalCategory = c.String(maxLength: 100),
                        modifiedOn = c.DateTime(),
                        modifiedBy = c.Guid(),
                        createdOn = c.DateTime(),
                        createdBy = c.Guid(),
                    })
                .PrimaryKey(t => t.glAccountAutoId);
            
            CreateTable(
                "dbo.item",
                c => new
                    {
                        itemId = c.Guid(nullable: false),
                        itemName = c.String(maxLength: 100),
                        itemNumber = c.String(maxLength: 100),
                        itemCategory1 = c.String(maxLength: 100),
                        itemCategory2 = c.String(maxLength: 100),
                        itemCategory3 = c.String(maxLength: 100),
                        modifiedOn = c.DateTime(),
                        modifiedBy = c.Guid(),
                        createdOn = c.DateTime(),
                        createdBy = c.Guid(),
                    })
                .PrimaryKey(t => t.itemId);
            
            CreateTable(
                "dbo.jobTitle",
                c => new
                    {
                        jobtitleId = c.Guid(nullable: false),
                        JobTitle = c.String(maxLength: 100),
                        description = c.String(maxLength: 200),
                        jobCode = c.String(maxLength: 100),
                        payrate = c.Decimal(storeType: "money"),
                        payrate_base = c.Decimal(storeType: "money"),
                        pOSID = c.String(maxLength: 100),
                        gLAccount_id = c.Guid(),
                        location_id = c.Guid(),
                        createdOn = c.DateTime(),
                        modifiedOn = c.DateTime(),
                        rating = c.Int(),
                        excludeFromSchedule = c.Boolean(),
                        excludeFromPOSImport = c.Boolean(),
                    })
                .PrimaryKey(t => t.jobtitleId);
            
            CreateTable(
                "dbo.laborDetail",
                c => new
                    {
                        laborId = c.Guid(nullable: false),
                        labor = c.String(maxLength: 100),
                        dateWorked = c.DateTime(),
                        endTime = c.DateTime(),
                        hours = c.Decimal(precision: 18, scale: 2),
                        payRate = c.Decimal(storeType: "money"),
                        payrate_Base = c.Decimal(storeType: "money"),
                        payrollStatus = c.Int(),
                        startTime = c.DateTime(),
                        total = c.Decimal(storeType: "money"),
                        total_Base = c.Decimal(storeType: "money"),
                        employee_ID = c.Guid(),
                        employeeJobTitle_ID = c.Guid(),
                        jobTitle_ID = c.Guid(),
                        location_ID = c.Guid(),
                        cateringEvent = c.Guid(),
                        createdOn = c.DateTime(),
                        modifiedOn = c.DateTime(),
                        tipDeclaredAmount = c.Decimal(storeType: "money"),
                        employee = c.String(maxLength: 100),
                        payrollID = c.String(maxLength: 100),
                        jobTitle = c.String(maxLength: 100),
                        dateWorkedDateText = c.DateTime(),
                        location = c.String(maxLength: 100),
                    })
                .PrimaryKey(t => t.laborId);
            
            CreateTable(
                "dbo.location",
                c => new
                    {
                        locationId = c.Guid(nullable: false),
                        legalEntityId = c.Guid(),
                        locationNo = c.String(maxLength: 7, unicode: false),
                        location_Number = c.String(maxLength: 100),
                        locationName = c.String(maxLength: 60),
                        legalEntityNumber = c.String(maxLength: 7, unicode: false),
                        legalEntity_Number = c.String(maxLength: 100),
                        legalEntityName = c.String(maxLength: 60),
                        attribute1Number = c.String(maxLength: 7, unicode: false),
                        attribute1Name = c.String(maxLength: 60),
                        attribute1Id = c.Guid(),
                        attribute2Number = c.String(maxLength: 7, unicode: false),
                        attribute2Name = c.String(maxLength: 60),
                        attribute2Id = c.Guid(),
                        locationModifiedOn = c.DateTime(),
                        modifiedOn = c.DateTime(),
                        modifiedBy = c.Guid(),
                        createdOn = c.DateTime(),
                        createdBy = c.Guid(),
                    })
                .PrimaryKey(t => t.locationId);
            
            CreateTable(
                "dbo.posEmployee",
                c => new
                    {
                        pOSEmployeeId = c.Guid(nullable: false),
                        fullName = c.String(maxLength: 100),
                        pOSID = c.String(maxLength: 100),
                        location_id = c.Guid(),
                        employee_id = c.Guid(),
                        createdOn = c.DateTime(),
                        modifiedOn = c.DateTime(),
                    })
                .PrimaryKey(t => t.pOSEmployeeId);
            
            CreateTable(
                "dbo.salesDetail",
                c => new
                    {
                        salesDetailId = c.Guid(nullable: false),
                        menuitem = c.String(maxLength: 100),
                        amount = c.Decimal(storeType: "money"),
                        customerPOSText = c.String(maxLength: 100),
                        date = c.DateTime(),
                        quantity = c.Decimal(precision: 18, scale: 2),
                        _void = c.Boolean(name: "void"),
                        company = c.Guid(),
                        location = c.Guid(),
                        salesID = c.Guid(),
                        salesAccount = c.String(maxLength: 250),
                        category = c.String(maxLength: 100),
                        houseaccounttransaction = c.Guid(),
                        transactionDetailId = c.Guid(),
                        cateringEvent = c.Guid(),
                    })
                .PrimaryKey(t => t.salesDetailId);
            
            CreateTable(
                "dbo.salesEmployee",
                c => new
                    {
                        salesId = c.Guid(nullable: false),
                        receiptNumber = c.String(maxLength: 100),
                        checkNumber = c.String(maxLength: 100),
                        comment = c.String(maxLength: 300),
                        date = c.DateTime(nullable: false),
                        dayOfWeek = c.String(maxLength: 9),
                        dayPart = c.String(maxLength: 100),
                        netSales = c.Decimal(nullable: false, storeType: "money"),
                        numberofGuests = c.Int(nullable: false),
                        orderHour = c.Int(nullable: false),
                        salesAmount = c.Decimal(nullable: false, storeType: "money"),
                        taxAmount = c.Decimal(nullable: false, storeType: "money"),
                        tipAmount = c.Decimal(nullable: false, storeType: "money"),
                        totalAmount = c.Decimal(nullable: false, storeType: "money"),
                        totalPayment = c.Decimal(nullable: false, storeType: "money"),
                        _void = c.Boolean(name: "void", nullable: false),
                        server = c.String(maxLength: 100),
                        location = c.Guid(),
                        grossSales = c.Decimal(nullable: false, storeType: "money"),
                    })
                .PrimaryKey(t => t.salesId);
            
            CreateTable(
                "dbo.salesPayment",
                c => new
                    {
                        salespaymentId = c.Guid(nullable: false),
                        name = c.String(maxLength: 100),
                        amount = c.Decimal(storeType: "money"),
                        comment = c.String(maxLength: 300),
                        customerPOSText = c.String(maxLength: 100),
                        date = c.DateTime(),
                        isException = c.Boolean(),
                        missingreceipt = c.Boolean(),
                        company = c.Guid(),
                        location = c.Guid(),
                        paymenttype = c.String(maxLength: 100),
                        salesID = c.Guid(),
                        salesAccount = c.String(maxLength: 250),
                        houseAccountTransaction = c.Guid(),
                        transactionDetailId = c.Guid(),
                        cateringEvent = c.Guid(),
                        exclude = c.Boolean(),
                    })
                .PrimaryKey(t => t.salespaymentId);
            
            CreateTable(
                "dbo.transaction",
                c => new
                    {
                        transactionId = c.Guid(nullable: false),
                        transactionLocationId = c.Guid(),
                        budgetId = c.Guid(),
                        transactionDate = c.DateTime(),
                        transactionNumber = c.String(maxLength: 100),
                        transactionName = c.String(maxLength: 200),
                        transactionType = c.String(maxLength: 4000),
                        lineItem = c.Byte(),
                        budgetActual = c.String(maxLength: 1, fixedLength: true, unicode: false),
                        transactionLocationName = c.String(maxLength: 100),
                        companyId = c.Guid(),
                        modifiedOn = c.DateTime(),
                        modifiedBy = c.Guid(),
                        createdOn = c.DateTime(),
                        createdBy = c.Guid(),
                    })
                .PrimaryKey(t => t.transactionId);
            
            CreateTable(
                "dbo.transactionDetail",
                c => new
                    {
                        transactionDetailAutoId = c.Long(nullable: false, identity: true),
                        transactionDetailId = c.Guid(nullable: false),
                        transactionId = c.Guid(nullable: false),
                        locationId = c.Guid(),
                        transactionLocationId = c.Guid(),
                        glAccountId = c.Guid(),
                        itemId = c.Guid(),
                        credit = c.Decimal(storeType: "money"),
                        debit = c.Decimal(storeType: "money"),
                        amount = c.Decimal(storeType: "money"),
                        quantity = c.Decimal(precision: 23, scale: 10),
                        previousCountTotal = c.Decimal(storeType: "money"),
                        adjustment = c.Decimal(storeType: "money"),
                        detailType = c.Int(),
                        unitOfMeasureName = c.String(maxLength: 100),
                        bankReconciliation = c.String(maxLength: 100),
                        bankDeposit = c.String(maxLength: 200),
                        comment = c.String(maxLength: 100),
                        unionId = c.Byte(nullable: false),
                        rowType = c.String(nullable: false, maxLength: 20),
                        modifiedOn = c.DateTime(),
                        modifiedBy = c.Guid(),
                        createdOn = c.DateTime(),
                        createdBy = c.Guid(),
                    })
                .PrimaryKey(t => t.transactionDetailAutoId);
            
            CreateTable(
                "dbo.userInfo",
                c => new
                    {
                        userId = c.Guid(nullable: false),
                        fullName = c.String(maxLength: 160),
                        modifiedOn = c.DateTime(),
                        modifiedBy = c.Guid(),
                        createdOn = c.DateTime(),
                        createdBy = c.Guid(),
                    })
                .PrimaryKey(t => t.userId);
            
            CreateTable(
                "dbo.userSecurity",
                c => new
                    {
                        userSecurityAutoId = c.Long(nullable: false, identity: true),
                        userId = c.Guid(),
                        locationId = c.Guid(),
                        userName = c.String(maxLength: 160),
                        locationName = c.String(maxLength: 100),
                    })
                .PrimaryKey(t => t.userSecurityAutoId);


            /*User tables*/
            CreateTable(
                "dbo.Roles",
                c => new
                {
                    RoleId = c.Guid(nullable: false),
                    Name = c.String(nullable: false, maxLength: 100),
                    ID = c.Int(nullable: false, identity: true),
                    PrimaryRole = c.Boolean(),
                    Description = c.String(),
                })
                .PrimaryKey(t => t.RoleId);

            CreateTable(
                "dbo.SystemUserBase",
                c => new
                {
                    SystemUserId = c.Guid(nullable: false),
                    TerritoryId = c.Guid(),
                    OrganizationId = c.Guid(),
                    BusinessUnitId = c.Guid(),
                    ParentSystemUserId = c.Guid(),
                    FirstName = c.String(maxLength: 50),
                    Salutation = c.String(maxLength: 20),
                    MiddleName = c.String(maxLength: 50),
                    LastName = c.String(maxLength: 50),
                    PersonalEMailAddress = c.String(maxLength: 100),
                    FullName = c.String(maxLength: 160),
                    NickName = c.String(maxLength: 50),
                    Title = c.String(maxLength: 100),
                    InternalEMailAddress = c.String(maxLength: 100),
                    JobTitle = c.String(maxLength: 100),
                    MobileAlertEMail = c.String(maxLength: 100),
                    PreferredEmailCode = c.Int(),
                    HomePhone = c.String(maxLength: 50),
                    MobilePhone = c.String(maxLength: 50),
                    PreferredPhoneCode = c.Int(),
                    PreferredAddressCode = c.Int(),
                    PhotoUrl = c.String(maxLength: 200),
                    DomainName = c.String(maxLength: 255),
                    PassportLo = c.Int(),
                    CreatedOn = c.DateTime(),
                    PassportHi = c.Int(),
                    DisabledReason = c.String(maxLength: 500),
                    ModifiedOn = c.DateTime(),
                    CreatedBy = c.Guid(),
                    EmployeeId = c.String(maxLength: 100),
                    ModifiedBy = c.Guid(),
                    IsDisabled = c.Boolean(),
                    GovernmentId = c.String(maxLength: 100),
                    VersionNumber = c.Binary(fixedLength: true, timestamp: true, storeType: "timestamp"),
                    Skills = c.String(maxLength: 100),
                    DisplayInServiceViews = c.Boolean(),
                    CalendarId = c.Guid(),
                    ActiveDirectoryGuid = c.Guid(),
                    SetupUser = c.Boolean(),
                    SiteId = c.Guid(),
                    WindowsLiveID = c.String(maxLength: 100),
                    IncomingEmailDeliveryMethod = c.Int(),
                    OutgoingEmailDeliveryMethod = c.Int(),
                    ImportSequenceNumber = c.Int(),
                    AccessMode = c.Int(),
                    InviteStatusCode = c.Int(),
                    IsActiveDirectoryUser = c.Boolean(),
                    OverriddenCreatedOn = c.DateTime(),
                    UTCConversionTimeZoneCode = c.Int(),
                    TimeZoneRuleVersionNumber = c.Int(),
                    YomiFullName = c.String(maxLength: 160),
                    YomiLastName = c.String(maxLength: 50),
                    YomiMiddleName = c.String(maxLength: 50),
                    YomiFirstName = c.String(maxLength: 50),
                    CreatedOnBehalfBy = c.Guid(),
                    ExchangeRate = c.Decimal(precision: 23, scale: 10),
                    IsIntegrationUser = c.Boolean(),
                    ModifiedOnBehalfBy = c.Guid(),
                    EmailRouterAccessApproval = c.Int(),
                    DefaultFiltersPopulated = c.Boolean(),
                    CALType = c.Int(),
                    QueueId = c.Guid(),
                    TransactionCurrencyId = c.Guid(),
                    IsSyncWithDirectory = c.Boolean(),
                    IsLicensed = c.Boolean(),
                    dm_EmailPassword = c.String(maxLength: 200),
                    dm_EmailOutGoingMailServer = c.String(maxLength: 200),
                    dm_EmailPort = c.String(maxLength: 200),
                    dm_EmailSSL = c.Boolean(),
                    dm_Login = c.String(maxLength: 200),
                    dm_LoginPassword = c.String(maxLength: 200),
                    dm_ChangePassword = c.Boolean(),
                    dm_AvailableToAllLocations = c.Boolean(),
                    dm_Inactive = c.Boolean(),
                    dm_R365 = c.Boolean(),
                    dm_Location = c.Guid(),
                    defaultPage = c.String(maxLength: 100),
                    DefaultDashboard = c.String(maxLength: 250),
                    dm_AllReports = c.Int(),
                    dm_NewFeature = c.Boolean(),
                    dm_Theme = c.Int(),
                    R365Manager = c.Int(),
                    FullAccess = c.Boolean(),
                })
                .PrimaryKey(t => t.SystemUserId);

            CreateTable(
                "dbo.UserRole",
                c => new
                {
                    UserId = c.Guid(nullable: false),
                    RoleId = c.Guid(nullable: false),
                    ID = c.Int(nullable: false, identity: true),
                })
                .PrimaryKey(t => new { t.UserId, t.RoleId });
        }
        
        public override void Down()
        {
            DropTable("dbo.userSecurity");
            DropTable("dbo.userInfo");
            DropTable("dbo.transactionDetail");
            DropTable("dbo.transaction");
            DropTable("dbo.salesPayment");
            DropTable("dbo.salesEmployee");
            DropTable("dbo.salesDetail");
            DropTable("dbo.posEmployee");
            DropTable("dbo.location");
            DropTable("dbo.laborDetail");
            DropTable("dbo.jobTitle");
            DropTable("dbo.item");
            DropTable("dbo.glAccount");
            DropTable("dbo.employee");
            DropTable("dbo.company");

            /*User tables*/
            DropTable("dbo.UserRole");
            DropTable("dbo.SystemUserBase");
            DropTable("dbo.Roles");
        }
    }
}
