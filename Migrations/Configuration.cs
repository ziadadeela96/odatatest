namespace R365.WebApi.Migrations
{
    using Bogus;
    using R365.WebApi.Areas.v1.Views.EF;
    using R365.WebApi.EF.R365;
    using System;
    using System.Data.Entity.Migrations;
    using System.Data.SqlClient;
    using System.IO;

    internal sealed class Configuration : DbMigrationsConfiguration<R365.WebApi.Areas.v1.Views.EF.ApiV1Context>
    {
        private int SeedsRecords = 5000;
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(ApiV1Context context)
        {
         
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.

            CompanySeeder(context);
            LocationSeeder(context);
            EmployeeSeeder(context);
            UserSeeder();
            
        }

        private void UserSeeder()
        {
            var context = new R365Context();

            context.Database.ExecuteSqlCommand("TRUNCATE TABLE [SystemUserBase]");

            var sql = @"Insert Into SystemUserBase (SystemUserId, FullAccess,IsDisabled,dm_login,dm_LoginPassword,dm_inactive) Values (@SystemUserId, @FullAccess,@IsDisabled,@dm_login,@dm_LoginPassword,@dm_inactive)";
            int noOfRowInserted = context.Database.ExecuteSqlCommand(sql,
                                    new SqlParameter("@SystemUserId", new Guid("00000000-0000-0000-0000-000000000001")),
                                    new SqlParameter("@FullAccess", true),
                                    new SqlParameter("@IsDisabled", false),
                                    new SqlParameter("@dm_login", "ziad"),
                                    new SqlParameter("@dm_LoginPassword", "123"),
                                    new SqlParameter("@dm_inactive", false)
                                    );


            //var users = new Faker<SystemUserBase>()

            //                .RuleFor(o => o.SystemUserId, new Guid("00000000-0000-0000-0000-000000000001"))
            //                .RuleFor(o => o.TerritoryId, f => Guid.NewGuid())
            //                .RuleFor(o => o.OrganizationId, f => Guid.NewGuid())
            //                .RuleFor(o => o.FirstName, f => f.Name.FirstName())
            //                .RuleFor(o => o.LastName, f => f.Name.LastName())
            //                .RuleFor(u => u.FullName, (f, u) => u.FirstName + " " + u.LastName)
            //                .RuleFor(o => o.VersionNumber, f => f.Random.Bytes(8))
            //                .RuleFor(o => o.FullAccess, true)
            //                .Generate(1);

            //context.SystemUserBase.AddOrUpdate(c => c.SystemUserId, users.ToArray());
        }

        private void LocationSeeder(ApiV1Context context){

            context.Database.ExecuteSqlCommand("TRUNCATE TABLE [Location]");
            var locations = new Faker<location>()
                            //Ensure all properties have rules. By default, StrictMode is false
                            //Set a global policy by using Faker.DefaultStrictMode
                            //.StrictMode(true)
                            //accountId is deterministic
                            .RuleFor(o => o.locationId, f => Guid.NewGuid())
                            .RuleFor(o => o.legalEntityId, f => Guid.NewGuid())
                            .RuleFor(o => o.locationNo, f => f.Random.Number(10).ToString())
                            .RuleFor(o => o.location_Number, f => f.Random.Number(10).ToString())
                            .RuleFor(o => o.locationName, f => f.Address.State())
                            .RuleFor(o => o.modifiedOn, f => f.Date.Recent())
                            .RuleFor(o => o.createdOn, f => f.Date.Recent())
                            .RuleFor(o => o.modifiedBy, f => null)
                            .RuleFor(o => o.createdBy, f => null)
                            .Generate(SeedsRecords);

            context.location.AddOrUpdate(c => c.locationId, locations.ToArray());
        }

        private void CompanySeeder(ApiV1Context context){

            context.Database.ExecuteSqlCommand("TRUNCATE TABLE [Company]");

            var companies = new Faker<company>()
                            //Ensure all properties have rules. By default, StrictMode is false
                            //Set a global policy by using Faker.DefaultStrictMode
                            .StrictMode(true)
                            //accountId is deterministic
                            .RuleFor(o => o.accountId, f => Guid.NewGuid())
                            //Pick some fruit from a basket
                            .RuleFor(o => o.companyName, f => f.Company.CompanyName())
                            //A random quantity from 1 to 10
                            .RuleFor(o => o.companyNumber, f => f.Random.Number(100000, 1000000).ToString())
                            .RuleFor(o => o.comment, f => f.Lorem.Sentence())
                            .RuleFor(o => o.modifiedOn, f => f.Date.Recent())
                            .RuleFor(o => o.createdOn, f => f.Date.Recent())
                            .RuleFor(o => o.modifiedBy, f => null)
                            .RuleFor(o => o.createdBy, f => null)
                            .Generate(SeedsRecords);

            context.company.AddOrUpdate(c => c.accountId, companies.ToArray());
        }


        private void EmployeeSeeder(ApiV1Context context)
        {

            context.Database.ExecuteSqlCommand("TRUNCATE TABLE [Employee]");

            var employees = new Faker<employee>()
                            .RuleFor(o => o.employeeId, f => Guid.NewGuid())
                            .RuleFor(o => o.FirstName, f => f.Name.FirstName())
                            .RuleFor(o => o.LastName, f => f.Name.LastName())
                            .RuleFor(o => o.FullName, (f, o) => o.FirstName + " " + o.LastName)
                            .RuleFor(o => o.Address1, f => f.Address.FullAddress())
                            .RuleFor(o => o.Address2, f => f.Address.FullAddress())
                            .RuleFor(o => o.AllowTextMessaging, f => f.Random.Bool())
                            .RuleFor(o => o.BirthdayDay, f => f.Random.Number(1,30))
                            .RuleFor(o => o.BirthdayMonth, f => f.Random.Number(1,12))
                            .RuleFor(o => o.City, f => f.Address.City())
                            .RuleFor(o => o.ModifiedOn, f => f.Date.Recent())
                            .RuleFor(o => o.CreatedOn, f => f.Date.Recent())
                            .Generate(SeedsRecords);

            context.employee.AddOrUpdate(c => c.employeeId, employees.ToArray());
        }

        //private void TransactionSeeder(ApiV1Context context)
        //{
        //    context.Database.ExecuteSqlCommand("TRUNCATE TABLE [Employee]");

        //    var transactions = new Faker<transaction>()
        //                    .RuleFor(o => o.transactionId, f => Guid.NewGuid())
        //                    .RuleFor(o => o.transactionName, f => f.Name.FullName())
        //                    .RuleFor(o => o.transactionNumber, f => f.Name.LastName())
        //                    .RuleFor(o => o.FullName, (f, o) => o.FirstName + " " + o.LastName)
        //                    .RuleFor(o => o.Address1, f => f.Address.FullAddress())
        //                    .RuleFor(o => o.Address2, f => f.Address.FullAddress())
        //                    .RuleFor(o => o.AllowTextMessaging, f => f.Random.Bool())
        //                    .RuleFor(o => o.BirthdayDay, f => f.Random.Number(1, 30))
        //                    .RuleFor(o => o.BirthdayMonth, f => f.Random.Number(1, 12))
        //                    .RuleFor(o => o.City, f => f.Address.City())
        //                    .RuleFor(o => o.ModifiedOn, f => f.Date.Recent())
        //                    .RuleFor(o => o.CreatedOn, f => f.Date.Recent())
        //                    .Generate(3000);

        //    context.transaction.AddOrUpdate(c => c.transactionId, transactions.ToArray());
        //}
    }
}
