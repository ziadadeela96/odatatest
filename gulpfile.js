﻿/// <binding AfterBuild='sass:build-css, scripts:build' ProjectOpened='sass:watch, scripts:watch' />
/*
This file is the main entry point for defining Gulp tasks and using Gulp plugins.
Click here to learn more. https://go.microsoft.com/fwlink/?LinkId=518007
*/

var gulp = require('gulp');
var sass = require('gulp-sass');
var ts = require('gulp-typescript');
var tsProject = ts.createProject('tsconfig.json');

gulp.task('sass:build-css', function () {
    return gulp.src('./content/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./content/'));
});

gulp.task('sass:watch', function () {
    gulp.watch('./content/**/*.scss', ['sass:build-css']);
});

gulp.task("scripts:build", function () {
    var tsResult = gulp.src("./Scripts/**/*.ts") // or tsProject.src()
        .pipe(tsProject());

    return tsResult.js.pipe(gulp.dest('./Scripts/dest/'));
});

gulp.task('scripts:watch', function () {
    gulp.watch('./Scripts/**/*.ts', ['scripts:build']);
});