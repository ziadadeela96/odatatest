﻿using System.Net.Http.Headers;
using System.Web.Http;

namespace R365.WebApi
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            //var container = new UnityContainer();
            ////container.RegisterType<Areas.v1.Views.EF.ApiV1Context>( new PerRequestLifetimeManager());
            //config.DependencyResolver = new UnityResolver(container);


            config.Routes.MapHttpRoute(
                name: "ApiAuthentication",
                routeTemplate: "api/Authentication/{id}",
                defaults: new { controller = "ApiAuthentication", id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
                name: "WebApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            


            config.Formatters.JsonFormatter.SupportedMediaTypes
             .Add(new MediaTypeHeaderValue("text/html"));

            //config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));
        }
    }
}
