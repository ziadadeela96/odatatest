﻿using Microsoft.OData;
using Microsoft.OData.Edm;
using Microsoft.OData.UriParser;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.OData.Builder;
using System.Web.OData.Extensions;
using System.Web.OData.Routing.Conventions;

namespace R365.WebApi.App_Start
{
    public static class ODataConfig
    {
        public const int MaxTop = 5000;
        public const int PageSize = MaxTop;
        public static void Register(HttpConfiguration config)
        {
            config.EnableDependencyInjection();

            //This has to be called before the following OData mapping, so also before WebApi mapping
            config.MapHttpAttributeRoutes();

            config.Select().Expand().Filter().OrderBy().MaxTop(MaxTop).Count();

            config.AddODataQueryFilter(new System.Web.OData.EnableQueryAttribute()
            {
                AllowedQueryOptions = System.Web.OData.Query.AllowedQueryOptions.All,
                PageSize = PageSize,
                MaxTop = MaxTop
            });


            // OData routes
            var builder = new ODataConventionModelBuilder();
            builder.EntitySet<Areas.v1.Views.Models.Company>("Company");
            builder.EntitySet<Areas.v1.Views.Models.GlAccount>("GlAccount");
            builder.EntitySet<Areas.v1.Views.Models.Item>("Item");
            builder.EntitySet<Areas.v1.Views.Models.Location>("Location");
            builder.EntitySet<Areas.v1.Views.Models.Transaction>("Transaction");
            builder.EntitySet<Areas.v1.Views.Models.TransactionDetail>("TransactionDetail");
            builder.EntitySet<Areas.v1.Views.Models.Employee>("Employee");
            builder.EntitySet<Areas.v1.Views.Models.JobTitle>("JobTitle");
            builder.EntitySet<Areas.v1.Views.Models.LaborDetail>("LaborDetail");
            builder.EntitySet<Areas.v1.Views.Models.PosEmployee>("PosEmployee");
            builder.EntitySet<Areas.v1.Views.Models.SalesDetail>("SalesDetail");
            builder.EntitySet<Areas.v1.Views.Models.SalesEmployee>("SalesEmployee");
            builder.EntitySet<Areas.v1.Views.Models.SalesPayment>("SalesPayment");

            builder.Namespace = typeof(Areas.v1.Views.EF.transaction).Namespace;
            builder.EnableLowerCamelCase();
            var edmModel = builder.GetEdmModel();


            //config.MapODataServiceRoute("V1RouteVersioning", "api/v1", edmModel);

            config.MapODataServiceRoute(routeName: "V1RouteVersioning1", routePrefix: "api/v1/views", configureAction: routeBuilder =>
                routeBuilder.AddService<IEdmModel>(ServiceLifetime.Singleton, sp => edmModel)
                    .AddService<IEnumerable<IODataRoutingConvention>>(ServiceLifetime.Singleton, sp => ODataRoutingConventions.CreateDefaultWithAttributeRouting("V1RouteVersioning", config))
                    .AddService<ODataUriResolver>(ServiceLifetime.Singleton, sp => new ODataUriResolver() { EnableCaseInsensitive = true }));

            config.MapODataServiceRoute(routeName: "V1RouteVersioning2", routePrefix: "ODataDev/api/v1/views", configureAction: routeBuilder =>
               routeBuilder.AddService<IEdmModel>(ServiceLifetime.Singleton, sp => edmModel)
                   .AddService<IEnumerable<IODataRoutingConvention>>(ServiceLifetime.Singleton, sp => ODataRoutingConventions.CreateDefaultWithAttributeRouting("V1RouteVersioning", config))
                   .AddService<ODataUriResolver>(ServiceLifetime.Singleton, sp => new ODataUriResolver() { EnableCaseInsensitive = true }));

        }
    }
}