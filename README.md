﻿## Steps to run the project


### Step 1: Make sure you have all the required dependencies. [DO NOT UPDATE THE DEPENDECIES USING NUGET]

- typescript 2.6
- Microsoft.AspNet.OData.6.1.0
- EntityFramework
- Faker.Net
- ...

### Step 2: Database configurations [Local DB]

- Make sure that sql server is running [you can check throught the running services]
- Create new connection[localhost] & new DB[loc_test].
- Test local connection using the following program: VTTestConnectionString
	- Use the following connection string: [Server=localhost;Database=loc_test;Integrated Security=SSPI;Trusted_Connection=True];
- Change DB configurations in the project according to your personal config by going to the class ApiV1Context and change the connection string. [Was made temporarily, should be made in a more professional way]
- Execute the following command to build the tables and execute the seeders: [Update-Database -verbos]
	- Migration tables can be found in [201906182252228_test.cs] in migrations folder.
	- Seeds can be found in [Configuration.cs] in migrations folder.
	- You may need to enable the migrations using the command: [Enable-Migrations]
- Check the local DB. There should be 15 tables. and 5000 dummy records in the tables: company, location, employee


### Step 3: Test if it works!

- Build and run the project [ctrl + F5]
- Try to access the following API: http://localhost:63439/api/acompany/ , It should return all the 5000 companies from your local DB.
- Try to access the following OData API: http://localhost:63439/api/v1/views/company/ , It should return all the 5000 companies from your local DB.