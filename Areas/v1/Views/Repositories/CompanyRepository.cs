﻿using AutoMapper.QueryableExtensions;
using R365.WebApi.Areas.v1.Views.EF;
using R365.WebApi.Authentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace R365.WebApi.Areas.v1.Views.Repositories
{
    public class CompanyRepository : RepositoryBase
    {
        public CompanyRepository(ApiV1Context context, UserIdentity user =null) : base(context, user)
        {
        }

        public IQueryable<Models.Company> GetCompanies()
        {
            return this._context.company.ProjectTo<Models.Company>(AutoMapperConfig.GetMapperConfiguration());
        }
    }
}