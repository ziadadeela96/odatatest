﻿using AutoMapper.QueryableExtensions;
using R365.WebApi.Areas.v1.Views.EF;
using R365.WebApi.Authentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace R365.WebApi.Areas.v1.Views.Repositories
{
    public class ItemRepository : RepositoryBase
    {
        public ItemRepository(ApiV1Context context, UserIdentity user) : base(context, user)
        {
        }

        public IQueryable<Models.Item> GetItems()
        {
            return this._context.item.ProjectTo<Models.Item>(AutoMapperConfig.GetMapperConfiguration());
        }
    }
}