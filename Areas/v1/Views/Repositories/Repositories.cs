﻿using R365.WebApi.Authentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace R365.WebApi.Areas.v1.Views.Repositories
{
    public class Repositories
    {
        private EF.ApiV1Context _context;

        private Lazy<CompanyRepository> _companyRepo;
        private Lazy<GlAccountRepository> _glAccountRepo;
        private Lazy<ItemRepository> _itemRepo;
        private Lazy<LocationRepository> _locationRepo;
        private Lazy<TransactionDetailRepository> _transactionDetailRepo;
        private Lazy<TransactionRepository> _transactionRepo;
        private Lazy<EmployeeRepository> _employeeRepo;
        private Lazy<JobTitleRepository> _jobTitleRepo;
        private Lazy<LaborDetailRepository> _laborDetailRepo;
        private Lazy<PosEmployeeRepository> _posEmployeeRepo;
        private Lazy<SalesDetailRepository> _salesDetailRepo;
        private Lazy<SalesEmployeeRepository> _salesEmployeeRepo;
        private Lazy<SalesPaymentRepository> _salesPaymentRepo;

        public Repositories(EF.ApiV1Context context, UserIdentity user=null)
        {
            _context = context;
            _companyRepo = new Lazy<CompanyRepository>(() => new CompanyRepository(context));
            _glAccountRepo = new Lazy<GlAccountRepository>(() => new GlAccountRepository(context, user));
            _itemRepo = new Lazy<ItemRepository>(() => new ItemRepository(context, user));
            _locationRepo = new Lazy<LocationRepository>(() => new LocationRepository(context, user));
            _transactionDetailRepo = new Lazy<TransactionDetailRepository>(() => new TransactionDetailRepository(context, user));
            _transactionRepo = new Lazy<TransactionRepository>(() => new TransactionRepository(context, user));
            _employeeRepo = new Lazy<EmployeeRepository>(() => new EmployeeRepository(context, user));
            _jobTitleRepo = new Lazy<JobTitleRepository>(() => new JobTitleRepository(context, user));
            _laborDetailRepo = new Lazy<LaborDetailRepository>(() => new LaborDetailRepository(context, user));
            _posEmployeeRepo = new Lazy<PosEmployeeRepository>(() => new PosEmployeeRepository(context, user));
            _salesDetailRepo = new Lazy<SalesDetailRepository>(() => new SalesDetailRepository(context, user));
            _salesEmployeeRepo = new Lazy<SalesEmployeeRepository>(() => new SalesEmployeeRepository(context, user));
            _salesPaymentRepo = new Lazy<SalesPaymentRepository>(() => new SalesPaymentRepository(context, user));
        }

        public CompanyRepository CompanyRepo => _companyRepo.Value;
        public GlAccountRepository GlAccountRepo => _glAccountRepo.Value;
        public ItemRepository ItemRepo => _itemRepo.Value;
        public LocationRepository LocationRepo => _locationRepo.Value;
        public TransactionDetailRepository TransactionDetailRepo => _transactionDetailRepo.Value;
        public TransactionRepository TransactionRepo => _transactionRepo.Value;
        public EmployeeRepository EmployeeRepo => _employeeRepo.Value;
        public JobTitleRepository JobTitleRepo => _jobTitleRepo.Value;
        public LaborDetailRepository LaborDetailRepo => _laborDetailRepo.Value;
        public PosEmployeeRepository PosEmployeeRepo => _posEmployeeRepo.Value;
        public SalesDetailRepository SalesDetailRepo => _salesDetailRepo.Value;
        public SalesEmployeeRepository SalesEmployeeRepo => _salesEmployeeRepo.Value;
        public SalesPaymentRepository SalesPaymentRepo => _salesPaymentRepo.Value;
    }
}