﻿using AutoMapper.QueryableExtensions;
using R365.WebApi.Areas.v1.Views.EF;
using R365.WebApi.Authentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace R365.WebApi.Areas.v1.Views.Repositories
{
    public class JobTitleRepository : RepositoryBase
    {
        public JobTitleRepository(ApiV1Context context, UserIdentity user) : base(context, user)
        {
        }

        public IQueryable<Models.JobTitle> GetJobTitles(Guid userId)
        {
            if ((this._user?.CanViewAllLocations ?? false) || (this._user?.IsGlobalAdmin ?? false))
                return (from j in this._context.jobTitle
                        select j).ProjectTo<Models.JobTitle>(AutoMapperConfig.GetMapperConfiguration());
            else
                return (from j in this._context.jobTitle
                        join u in this._context.userSecurity on j.Location_id equals u.locationId
                        where u.userId == userId
                        select j).ProjectTo<Models.JobTitle>(AutoMapperConfig.GetMapperConfiguration());
        }
    }
}