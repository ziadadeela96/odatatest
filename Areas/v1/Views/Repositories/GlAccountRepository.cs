﻿using AutoMapper.QueryableExtensions;
using R365.WebApi.Areas.v1.Views.EF;
using R365.WebApi.Authentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace R365.WebApi.Areas.v1.Views.Repositories
{
    public class GlAccountRepository : RepositoryBase
    {
        public GlAccountRepository(ApiV1Context context, UserIdentity user) : base(context, user)
        {
        }

        public IQueryable<Models.GlAccount> GetGlAccounts(Guid userId)
        {
            if ((this._user?.CanViewAllLocations ?? false) || (this._user?.IsGlobalAdmin ?? false))
                return (from a in this._context.glAccount
                        select a).ProjectTo<Models.GlAccount>(AutoMapperConfig.GetMapperConfiguration());
            else
                return (from a in this._context.glAccount
                        join u in this._context.userSecurity on a.locationId equals u.locationId
                        where u.userId == userId
                        select a).ProjectTo<Models.GlAccount>(AutoMapperConfig.GetMapperConfiguration());
        }
    }
}