﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper.QueryableExtensions;
using R365.WebApi.Areas.v1.Views.Models;
using R365.WebApi.Areas.v1.Views.EF;
using R365.WebApi.Authentication;

namespace R365.WebApi.Areas.v1.Views.Repositories
{
    public class TransactionDetailRepository : RepositoryBase
    {
        public TransactionDetailRepository(ApiV1Context context, UserIdentity user) : base(context, user)
        {
        }

        public IQueryable<TransactionDetail> GetTransactionDetails(Guid userId)
        {
            if ((this._user?.CanViewAllLocations ?? false) || (this._user?.IsGlobalAdmin ?? false))
                return (from td in this._context.transactionDetail
                        select td).ProjectTo<TransactionDetail>(AutoMapperConfig.GetMapperConfiguration());
            else
                return (from td in this._context.transactionDetail
                        join u in this._context.userSecurity on td.transactionLocationId equals u.locationId
                        where u.userId == userId
                        select td).ProjectTo<TransactionDetail>(AutoMapperConfig.GetMapperConfiguration());
        }
    }
}