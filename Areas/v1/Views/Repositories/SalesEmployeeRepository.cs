﻿using AutoMapper.QueryableExtensions;
using R365.WebApi.Areas.v1.Views.EF;
using R365.WebApi.Authentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace R365.WebApi.Areas.v1.Views.Repositories
{
    public class SalesEmployeeRepository : RepositoryBase
    {
        public SalesEmployeeRepository(ApiV1Context context, UserIdentity user) : base(context, user)
        {
        }

        public IQueryable<Models.SalesEmployee> GetEmployees(Guid userId)
        {
            return this._context.salesEmployee.ProjectTo<Models.SalesEmployee>(AutoMapperConfig.GetMapperConfiguration());
        }
    }
}