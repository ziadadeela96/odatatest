﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using System.Web.OData.Query;
using System.Web.OData;
using AutoMapper.QueryableExtensions;
using R365.WebApi.Areas.v1.Views.Models;
using R365.WebApi.Areas.v1.Views.EF;
using R365.WebApi.Authentication;

namespace R365.WebApi.Areas.v1.Views.Repositories
{
    public class TransactionRepository : RepositoryBase
    {
        public TransactionRepository(ApiV1Context context, UserIdentity user) : base(context, user)
        {
        }

        public IQueryable<Transaction> GetTransactions(Guid userId)
        {
            if ((this._user?.CanViewAllLocations ?? false) || (this._user?.IsGlobalAdmin ?? false))
                return (from t in this._context.transaction
                        select t).ProjectTo<Transaction>(AutoMapperConfig.GetMapperConfiguration());
            else
                return (from t in this._context.transaction
                        join u in this._context.userSecurity on t.transactionLocationId equals u.locationId
                        where u.userId == userId
                        select t).ProjectTo<Transaction>(AutoMapperConfig.GetMapperConfiguration());
        }
    }
}