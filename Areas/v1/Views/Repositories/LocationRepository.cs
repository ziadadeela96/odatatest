﻿using AutoMapper.QueryableExtensions;
using R365.WebApi.Areas.v1.Views.EF;
using R365.WebApi.Authentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace R365.WebApi.Areas.v1.Views.Repositories
{
    public class LocationRepository : RepositoryBase
    {
        public LocationRepository(ApiV1Context context, UserIdentity user) : base(context, user)
        {
        }

        public IQueryable<Models.Location> GetLocations(Guid userId)
        {
            if ((this._user?.CanViewAllLocations ?? false) || (this._user?.IsGlobalAdmin ?? false))
                return (from l in this._context.location
                        select l).ProjectTo<Models.Location>(AutoMapperConfig.GetMapperConfiguration());
            else
                return (from l in this._context.location
                        join u in this._context.userSecurity on l.locationId equals u.locationId
                        where u.userId == userId
                        select l).ProjectTo<Models.Location>(AutoMapperConfig.GetMapperConfiguration());
        }
    }
}