﻿using R365.WebApi.Authentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace R365.WebApi.Areas.v1.Views.Repositories
{
    public class RepositoryBase
    {
        protected EF.ApiV1Context _context;
        protected UserIdentity _user;
        public RepositoryBase(EF.ApiV1Context context, UserIdentity user)
        {
            _context = context;
            _user = user;
        }
    }
}