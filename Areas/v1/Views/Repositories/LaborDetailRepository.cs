﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper.QueryableExtensions;
using R365.WebApi.Areas.v1.Views.Models;
using R365.WebApi.Areas.v1.Views.EF;
using R365.WebApi.Authentication;

namespace R365.WebApi.Areas.v1.Views.Repositories
{
    public class LaborDetailRepository : RepositoryBase
    {
        public LaborDetailRepository(ApiV1Context context, UserIdentity user) : base(context, user)
        {
        }

        public IQueryable<LaborDetail> GetLaborDetails(Guid userId)
        {
            if ((this._user?.CanViewAllLocations ?? false) || (this._user?.IsGlobalAdmin ?? false))
                return (from ld in this._context.laborDetail
                        select ld).ProjectTo<LaborDetail>(AutoMapperConfig.GetMapperConfiguration());
            else
                return (from ld in this._context.laborDetail
                        join u in this._context.userSecurity on ld.Location_ID equals u.locationId
                        where u.userId == userId
                        select ld).ProjectTo<LaborDetail>(AutoMapperConfig.GetMapperConfiguration());
        }
    }
}