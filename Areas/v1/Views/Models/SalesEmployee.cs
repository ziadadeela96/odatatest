﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace R365.WebApi.Areas.v1.Views.Models
{
    public class SalesEmployee : ModelBase
    {
        [Key]
        public Guid SalesId { get; set; }
        public string ReceiptNumber { get; set; }
        public string CheckNumber { get; set; }
        public string Comment { get; set; }
        public DateTime Date { get; set; }
        public string DayOfWeek { get; set; }
        public string DayPart { get; set; }
        public decimal NetSales { get; set; }
        public int NumberofGuests { get; set; }
        public int OrderHour { get; set; }
        public decimal SalesAmount { get; set; }
        public decimal TaxAmount { get; set; }
        public decimal TipAmount { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal TotalPayment { get; set; }
        public bool Void { get; set; }
        public string Server { get; set; }
        public Guid? Location { get; set; }
        public decimal GrossSales { get; set; }
    }
}