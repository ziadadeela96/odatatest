﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace R365.WebApi.Areas.v1.Views.Models
{
    public class GlAccount : ModelBase
    {
        [Key]
        public long GlAccountAutoId { get; set; }
        public Guid? GlAccountId { get; set; }
        public string LocationNumber { get; set; }
        public string LocationName { get; set; }
        public Guid? LocationId { get; set; }
        public string LegalEntityNumber { get; set; }
        public string LegalEntityName { get; set; }
        public Guid? LegalEntityId { get; set; }
        public string Attribute1Number { get; set; }
        public string Attribute1Name { get; set; }
        public Guid? Attribute1Id { get; set; }
        public string Attribute2Number { get; set; }
        public string Attribute2Name { get; set; }
        public Guid? Attribute2Id { get; set; }
        public string Name { get; set; }
        public string GlAccountNumber { get; set; }
        public int? GlTypeClass { get; set; }
        public string GlType { get; set; }
        public string OperationalCategory { get; set; }
    }
}