﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace R365.WebApi.Areas.v1.Views.Models
{
    public class Company : ModelBase
    {
        public Guid CompanyId { get; set; }
        public string Name { get; set; }
        public string CompanyNumber { get; set; }
        public string Comment { get; set; }
    }
}