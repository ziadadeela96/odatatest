﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace R365.WebApi.Areas.v1.Views.Models
{
    public class JobTitle : ModelBase
    {
        public Guid JobTitleId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string JobCode { get; set; }
        public decimal? PayRate { get; set; }
        public decimal? PayRate_Base { get; set; }
        public string POSID { get; set; }
        public Guid? GLAccount_Id { get; set; }
        public Guid? Location_Id { get; set; }
        public int? Rating { get; set; }
        public bool? ExcludeFromSchedule { get; set; }
        public bool? ExcludeFromPOSImport { get; set; }

    }
}