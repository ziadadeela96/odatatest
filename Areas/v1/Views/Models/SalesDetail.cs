﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace R365.WebApi.Areas.v1.Views.Models
{
    public class SalesDetail : ModelBase
    {
        public Guid? salesdetailID { get; set; }
        public string menuitem { get; set; }
        public decimal Amount { get; set; }
        public string CustomerPOSText { get; set; }
        public DateTime Date { get; set; }
        public decimal Quantity { get; set; }
        public bool? Void { get; set; }
        public Guid? Company { get; set; }
        public Guid? Location { get; set; }
        public Guid? SalesID { get; set; }
        public string SalesAccount { get; set; }
        public string Category { get; set; }
        public Guid? HouseAccountTransaction { get; set; }
        public Guid? TransactionDetailID { get; set; }
        public Guid? CateringEvent { get; set; }
    }
}