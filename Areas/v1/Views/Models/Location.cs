﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace R365.WebApi.Areas.v1.Views.Models
{
    public class Location : ModelBase
    {
        public Guid LocationId { get; set; }
        public string Name { get; set; }
        public string LocationNumber { get; set; }
        public Guid? LegalEntityId { get; set; }
        public string LegalEntityNumber { get; set; }
        public string LegalEntityName { get; set; }
        public Guid? Attribute1Id { get; set; }
        public string Attribute1Number { get; set; }
        public string Attribute1Name { get; set; }
        public Guid? Attribute2Id { get; set; }
        public string Attribute2Number { get; set; }
        public string Attribute2Name { get; set; }
    }
}