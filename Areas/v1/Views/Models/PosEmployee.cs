﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace R365.WebApi.Areas.v1.Views.Models
{
    public class PosEmployee : ModelBase
    {
        public Guid POSEmployeeId { get; set; }
        public string FullName { get; set; }
        public string POSID { get; set; }
        public Guid? Location_id { get; set; }
        public Guid? Employee_id { get; set; }
    }
}