﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace R365.WebApi.Areas.v1.Views.Models
{
    public class SalesPayment : ModelBase
    {
        public Guid? salespaymentId { get; set; }
        public string name { get; set; }
        public decimal Amount { get; set; }
        public string Comment { get; set; }
        public string CustomerPOSText { get; set; }
        public DateTime Date { get; set; }
        public bool? IsException { get; set; }
        public bool? missingreceipt { get; set; }
        public Guid? Company { get; set; }
        public Guid? Location { get; set; }
        public string paymenttype { get; set; }
        public Guid? SalesID { get; set; }
        public Guid? HouseAccountTransaction { get; set; }
        public Guid? TransactionDetailID { get; set; }
        public Guid? CateringEvent { get; set; }
        public bool? exclude { get; set; }
    }
}