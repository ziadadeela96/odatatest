﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace R365.WebApi.Areas.v1.Views.Models
{
    public class Item : ModelBase
    {
        public Guid ItemId { get; set; }
        public string Name { get; set; }
        public string ItemNumber { get; set; }
        public string Category1 { get; set; }
        public string Category2 { get; set; }
        public string Category3 { get; set; }
    }
}