﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace R365.WebApi.Areas.v1.Views.Models
{
    public class LaborDetail : ModelBase
    {
        [Key]
        public Guid? laborId { get; set; }
        public string Labor { get; set; }
        public DateTime? DateWorked { get; set; }
        public DateTime? EndTime { get; set; }
        public decimal? Hours { get; set; }
        public decimal? PayRate { get; set; }
        public decimal? payrate_Base { get; set; }
        public int? PayrollStatus { get; set; }
        public DateTime? StartTime { get; set; }
        public decimal? Total { get; set; }
        public decimal? total_Base { get; set; }
        public Guid? Employee_ID { get; set; }
        public Guid? EmployeeJobTitle_ID { get; set; }
        public Guid? JobTitle_ID { get; set; }
        public Guid? Location_ID { get; set; }
        public Guid? CateringEvent { get; set; }
        public decimal? TipDeclaredAmount { get; set; }
        public string Employee { get; set; }
        public string PayrollID { get; set; }
        public string JobTitle { get; set; }
        public DateTime? DateWorkedDateText { get; set; }
        public string Location { get; set; }
    }
}