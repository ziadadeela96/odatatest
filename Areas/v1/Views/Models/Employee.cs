﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace R365.WebApi.Areas.v1.Views.Models
{
    public class Employee : ModelBase
    {
        public Guid? EmployeeId { get; set; }
        public string FullName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public bool? AllowTextMessaging { get; set; }
        public int? BirthdayDay { get; set; }
        public int? BirthdayMonth { get; set; }
        public string City { get; set; }
        public string FirstName { get; set; }
        public DateTime? HireDate { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string MobilePhone { get; set; }
        public int? MultipleLocations { get; set; }
        public string PayrollID { get; set; }
        public string PhoneNumber { get; set; }
        public string POSID { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public Guid? EmployeeLink { get; set; }
        public Guid? PrimaryLocation_id { get; set; }
        public bool? Inactive { get; set; }
        public string email { get; set; }
        public DateTime? Birthday { get; set; }
    }
}