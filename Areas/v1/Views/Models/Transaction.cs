﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace R365.WebApi.Areas.v1.Views.Models
{
    public class Transaction : ModelBase
    {
        public Guid TransactionId { get; set; }
        public Guid? LocationId { get; set; }
        public string LocationName { get; set; }
        public DateTime? Date { get; set; }
        public string TransactionNumber { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public Guid? CompanyId { get; set; }
    }
}