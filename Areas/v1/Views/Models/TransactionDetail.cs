﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace R365.WebApi.Areas.v1.Views.Models
{
    public class TransactionDetail : ModelBase
    {
        public long TransactionDetailAutoId { get; set; }
        public Guid TransactionDetailId { get; set; }
        public Guid TransactionId { get; set; }
        public Guid? LocationId { get; set; }
        public Guid? GlAccountId { get; set; }
        public Guid? ItemId { get; set; }
        public decimal? Credit { get; set; }
        public decimal? Debit { get; set; }
        public decimal? Amount { get; set; }
        public decimal? Quantity { get; set; }
        public decimal? PreviousCountTotal { get; set; }
        public decimal? Adjustment { get; set; }
        public string UnitOfMeasureName { get; set; }
        public string BankReconciliation { get; set; }
        public string BankDeposit { get; set; }
        public string Comment { get; set; }
        public string RowType { get; set; }
    }
}