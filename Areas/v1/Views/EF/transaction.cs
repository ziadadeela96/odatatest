namespace R365.WebApi.Areas.v1.Views.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("transaction")]
    public partial class transaction
    {
        public Guid transactionId { get; set; }

        public Guid? transactionLocationId { get; set; }

        public Guid? budgetId { get; set; }

        public DateTime? transactionDate { get; set; }

        [StringLength(100)]
        public string transactionNumber { get; set; }

        [StringLength(200)]
        public string transactionName { get; set; }

        [StringLength(4000)]
        public string transactionType { get; set; }

        public byte? lineItem { get; set; }

        [StringLength(1)]
        public string budgetActual { get; set; }

        [StringLength(100)]
        public string transactionLocationName { get; set; }

        public Guid? companyId { get; set; }

        public DateTime? modifiedOn { get; set; }

        public Guid? modifiedBy { get; set; }

        public DateTime? createdOn { get; set; }

        public Guid? createdBy { get; set; }
    }
}
