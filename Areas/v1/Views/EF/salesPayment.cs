namespace R365.WebApi.Areas.v1.Views.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("salesPayment")]
    public partial class salesPayment
    {
        public Guid? salespaymentId { get; set; }

        [StringLength(100)]
        public string name { get; set; }

        [Column(TypeName = "money")]
        public decimal? Amount { get; set; }

        [StringLength(300)]
        public string Comment { get; set; }

        [StringLength(100)]
        public string CustomerPOSText { get; set; }

        public DateTime? Date { get; set; }

        public bool? IsException { get; set; }

        public bool? missingreceipt { get; set; }

        public Guid? Company { get; set; }

        public Guid? Location { get; set; }

        [StringLength(100)]
        public string paymenttype { get; set; }

        public Guid? SalesID { get; set; }

        [StringLength(250)]
        public string SalesAccount { get; set; }

        public Guid? HouseAccountTransaction { get; set; }

        public Guid? TransactionDetailId { get; set; }

        public Guid? CateringEvent { get; set; }

        public bool? exclude { get; set; }
    }
}
