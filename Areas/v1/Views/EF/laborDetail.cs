namespace R365.WebApi.Areas.v1.Views.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("laborDetail")]
    public partial class laborDetail
    {
        [Key]
        public Guid? laborId { get; set; }

        [StringLength(100)]
        public string Labor { get; set; }

        public DateTime? DateWorked { get; set; }

        public DateTime? EndTime { get; set; }

        public decimal? Hours { get; set; }

        [Column(TypeName = "money")]
        public decimal? PayRate { get; set; }

        [Column(TypeName = "money")]
        public decimal? payrate_Base { get; set; }

        public int? PayrollStatus { get; set; }

        public DateTime? StartTime { get; set; }

        [Column(TypeName = "money")]
        public decimal? Total { get; set; }

        [Column(TypeName = "money")]
        public decimal? total_Base { get; set; }

        public Guid? Employee_ID { get; set; }

        public Guid? EmployeeJobTitle_ID { get; set; }

        public Guid? JobTitle_ID { get; set; }

        public Guid? Location_ID { get; set; }

        public Guid? CateringEvent { get; set; }

        public DateTime? CreatedOn { get; set; }

        public DateTime? ModifiedOn { get; set; }

        [Column(TypeName = "money")]
        public decimal? TipDeclaredAmount { get; set; }

        [StringLength(100)]
        public string Employee { get; set; }

        [StringLength(100)]
        public string PayrollID { get; set; }

        [StringLength(100)]
        public string JobTitle { get; set; }

        public DateTime? DateWorkedDateText { get; set; }

        [StringLength(100)]
        public string Location { get; set; }
    }
}
