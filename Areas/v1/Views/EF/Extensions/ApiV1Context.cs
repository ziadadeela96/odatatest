namespace R365.WebApi.Areas.v1.Views.EF
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Diagnostics;
    using System.Web.Configuration;

    public partial class ApiV1Context : DbContext
    {
        //Data Source=tcp:r365sql001.database.windows.net,1433;Initial Catalog=krgStage;User Id=ApiReadonly@r365sql001.database.windows.net;Password=R365Ap1r38d0nly1!;
        public ApiV1Context(string server, string dbName)
            : base($"Server=localhost;Database=loc_test;Integrated Security=SSPI;")
        //: base($"Server={serverIpAddress};Database={dbName};User Id={userName};Password={password};")
        //: base($"Server={serverIpAddress};Database={dbName};User Id={userName};Password={password};")
        {
            this.Database.CommandTimeout = (5 * 60); // 5 Mins
        }

        public ApiV1Context()
            : base($"Server=localhost;Database={WebConfigurationManager.AppSettings["DbName"]};Integrated Security=SSPI;")
        { }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            this.OnModelCreatingInt(modelBuilder);

            modelBuilder.Properties().Configure(c =>
            {
                var name = c.ClrPropertyInfo.Name;
                var newName = char.ToLower(name[0]) + name.Substring(1);
                c.HasColumnName(newName);
            });

            if (Debugger.IsAttached)
                System.Data.Entity.Infrastructure.Interception.DbInterception.Add(new QueryInterceptor());

            //modelBuilder.Entity<transactionDetail>()
            //    .HasRequired<transaction>(t => t.transaction)
            //    .WithMany(td => td.transactionDetails)
            //    .HasForeignKey<Guid>(td => td.transactionId);

        }
    }
}
