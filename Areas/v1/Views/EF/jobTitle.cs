namespace R365.WebApi.Areas.v1.Views.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("jobTitle")]
    public partial class jobTitle
    {
        public Guid jobtitleId { get; set; }

        [Column("JobTitle")]
        [StringLength(100)]
        public string Name { get; set; }

        [StringLength(200)]
        public string Description { get; set; }

        [StringLength(100)]
        public string JobCode { get; set; }

        [Column(TypeName = "money")]
        public decimal? payrate { get; set; }

        [Column(TypeName = "money")]
        public decimal? payrate_base { get; set; }

        [StringLength(100)]
        public string POSID { get; set; }

        public Guid? GLAccount_id { get; set; }

        public Guid? Location_id { get; set; }

        public DateTime? CreatedOn { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public int? Rating { get; set; }

        public bool? ExcludeFromSchedule { get; set; }

        public bool? ExcludeFromPOSImport { get; set; }
    }
}
