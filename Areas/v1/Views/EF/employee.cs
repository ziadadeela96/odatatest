namespace R365.WebApi.Areas.v1.Views.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("employee")]
    public partial class employee
    {
        public Guid? employeeId { get; set; }

        [StringLength(100)]
        public string FullName { get; set; }

        [StringLength(100)]
        public string Address1 { get; set; }

        [StringLength(100)]
        public string Address2 { get; set; }

        public bool? AllowTextMessaging { get; set; }

        public int? BirthdayDay { get; set; }

        public int? BirthdayMonth { get; set; }

        [StringLength(100)]
        public string City { get; set; }

        [StringLength(100)]
        public string FirstName { get; set; }

        public DateTime? HireDate { get; set; }

        [StringLength(100)]
        public string LastName { get; set; }

        [StringLength(100)]
        public string MiddleName { get; set; }

        [StringLength(100)]
        public string MobilePhone { get; set; }

        public int? MultipleLocations { get; set; }

        [StringLength(100)]
        public string PayrollID { get; set; }

        [StringLength(100)]
        public string PhoneNumber { get; set; }

        [StringLength(100)]
        public string POSID { get; set; }

        [StringLength(100)]
        public string State { get; set; }

        [StringLength(100)]
        public string ZipCode { get; set; }

        public Guid? EmployeeLink { get; set; }
        
        public Guid? PrimaryLocation_id { get; set; }

        public DateTime? CreatedOn { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public bool? Inactive { get; set; }

        [StringLength(200)]
        public string email { get; set; }

        public DateTime? Birthday { get; set; }
    }
}
