namespace R365.WebApi.Areas.v1.Views.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("transactionDetail")]
    public partial class transactionDetail
    {
        [Key]
        public long transactionDetailAutoId { get; set; }

        public Guid transactionDetailId { get; set; }

        public Guid transactionId { get; set; }

        public Guid? locationId { get; set; }

        public Guid? transactionLocationId { get; set; }

        public Guid? glAccountId { get; set; }

        public Guid? itemId { get; set; }

        [Column(TypeName = "money")]
        public decimal? credit { get; set; }

        [Column(TypeName = "money")]
        public decimal? debit { get; set; }

        [Column(TypeName = "money")]
        public decimal? amount { get; set; }

        public decimal? quantity { get; set; }

        [Column(TypeName = "money")]
        public decimal? previousCountTotal { get; set; }

        [Column(TypeName = "money")]
        public decimal? adjustment { get; set; }

        public int? detailType { get; set; }

        [StringLength(100)]
        public string unitOfMeasureName { get; set; }

        [StringLength(100)]
        public string bankReconciliation { get; set; }

        [StringLength(200)]
        public string bankDeposit { get; set; }

        [StringLength(100)]
        public string comment { get; set; }

        public byte unionId { get; set; }

        [Required]
        [StringLength(20)]
        public string rowType { get; set; }

        public DateTime? modifiedOn { get; set; }

        public Guid? modifiedBy { get; set; }

        public DateTime? createdOn { get; set; }

        public Guid? createdBy { get; set; }
    }
}
