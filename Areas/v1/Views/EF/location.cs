namespace R365.WebApi.Areas.v1.Views.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("location")]
    public partial class location
    {
        public Guid locationId { get; set; }

        public Guid? legalEntityId { get; set; }

        [StringLength(7)]
        public string locationNo { get; set; }

        [StringLength(100)]
        public string location_Number { get; set; }

        [StringLength(60)]
        public string locationName { get; set; }

        [StringLength(7)]
        public string legalEntityNumber { get; set; }

        [StringLength(100)]
        public string legalEntity_Number { get; set; }

        [StringLength(60)]
        public string legalEntityName { get; set; }

        [StringLength(7)]
        public string attribute1Number { get; set; }

        [StringLength(60)]
        public string attribute1Name { get; set; }

        public Guid? attribute1Id { get; set; }

        [StringLength(7)]
        public string attribute2Number { get; set; }

        [StringLength(60)]
        public string attribute2Name { get; set; }

        public Guid? attribute2Id { get; set; }

        public DateTime? locationModifiedOn { get; set; }

        public DateTime? modifiedOn { get; set; }

        public Guid? modifiedBy { get; set; }

        public DateTime? createdOn { get; set; }

        public Guid? createdBy { get; set; }
    }
}
