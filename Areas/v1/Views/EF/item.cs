namespace R365.WebApi.Areas.v1.Views.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("item")]
    public partial class item
    {
        public Guid itemId { get; set; }

        [StringLength(100)]
        public string itemName { get; set; }

        [StringLength(100)]
        public string itemNumber { get; set; }

        [StringLength(100)]
        public string itemCategory1 { get; set; }

        [StringLength(100)]
        public string itemCategory2 { get; set; }

        [StringLength(100)]
        public string itemCategory3 { get; set; }

        public DateTime? modifiedOn { get; set; }

        public Guid? modifiedBy { get; set; }

        public DateTime? createdOn { get; set; }

        public Guid? createdBy { get; set; }
    }
}
