namespace R365.WebApi.Areas.v1.Views.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("glAccount")]
    public partial class glAccount
    {
        [Key]
        public long glAccountAutoId { get; set; }

        [StringLength(7)]
        public string locationNo { get; set; }

        [StringLength(60)]
        public string locationName { get; set; }

        public Guid? locationId { get; set; }

        [StringLength(7)]
        public string legalEntityNumber { get; set; }

        [StringLength(60)]
        public string legalEntityName { get; set; }

        public Guid? legalEntityId { get; set; }

        [StringLength(7)]
        public string attribute1Number { get; set; }

        [StringLength(60)]
        public string attribute1Name { get; set; }

        public Guid? attribute1Id { get; set; }

        [StringLength(7)]
        public string attribute2Number { get; set; }

        [StringLength(60)]
        public string attribute2Name { get; set; }

        public Guid? attribute2Id { get; set; }

        public Guid? glAccountId { get; set; }

        [StringLength(100)]
        public string glAccountName { get; set; }

        [StringLength(13)]
        public string glAccountNumber { get; set; }

        [StringLength(100)]
        public string glAccount_Number { get; set; }

        public int? glTypeClass { get; set; }

        [StringLength(60)]
        public string glType { get; set; }

        [StringLength(100)]
        public string operationalCategory { get; set; }

        public DateTime? modifiedOn { get; set; }

        public Guid? modifiedBy { get; set; }

        public DateTime? createdOn { get; set; }

        public Guid? createdBy { get; set; }
    }
}
