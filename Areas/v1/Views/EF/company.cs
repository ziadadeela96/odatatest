namespace R365.WebApi.Areas.v1.Views.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("company")]
    public partial class company
    {
        [StringLength(160)]
        public string companyName { get; set; }

        [StringLength(100)]
        public string companyNumber { get; set; }

        [StringLength(200)]
        public string comment { get; set; }

        [Key]
        public Guid accountId { get; set; }

        public DateTime? modifiedOn { get; set; }

        public Guid? modifiedBy { get; set; }

        public DateTime? createdOn { get; set; }

        public Guid? createdBy { get; set; }
    }
}
