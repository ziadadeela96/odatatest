namespace R365.WebApi.Areas.v1.Views.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("userSecurity")]
    public partial class userSecurity
    {
        [Key]
        public long userSecurityAutoId { get; set; }

        public Guid? userId { get; set; }

        public Guid? locationId { get; set; }

        [StringLength(160)]
        public string userName { get; set; }

        [StringLength(100)]
        public string locationName { get; set; }
    }
}
