namespace R365.WebApi.Areas.v1.Views.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("salesEmployee")]
    public partial class salesEmployee
    {
        [Key]
        public Guid salesId { get; set; }

        [StringLength(100)]
        public string ReceiptNumber { get; set; }

        [StringLength(100)]
        public string CheckNumber { get; set; }

        [StringLength(300)]
        public string Comment { get; set; }

        public DateTime Date { get; set; }

        [StringLength(9)]
        public string DayOfWeek { get; set; }

        [StringLength(100)]
        public string DayPart { get; set; }

        [Column(TypeName = "money")]
        public decimal NetSales { get; set; }

        public int NumberofGuests { get; set; }

        public int OrderHour { get; set; }

        [Column(TypeName = "money")]
        public decimal SalesAmount { get; set; }

        [Column(TypeName = "money")]
        public decimal TaxAmount { get; set; }

        [Column(TypeName = "money")]
        public decimal TipAmount { get; set; }

        [Column(TypeName = "money")]
        public decimal TotalAmount { get; set; }

        [Column(TypeName = "money")]
        public decimal TotalPayment { get; set; }

        public bool Void { get; set; }

        [StringLength(100)]
        public string Server { get; set; }

        public Guid? Location { get; set; }

        [Column(TypeName = "money")]
        public decimal GrossSales { get; set; }
    }
}
