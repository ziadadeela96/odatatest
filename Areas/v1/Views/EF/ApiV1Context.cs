namespace R365.WebApi.Areas.v1.Views.EF
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class ApiV1Context : DbContext
    {
        public virtual DbSet<company> company { get; set; }
        public virtual DbSet<glAccount> glAccount { get; set; }
        public virtual DbSet<item> item { get; set; }
        public virtual DbSet<location> location { get; set; }
        public virtual DbSet<transaction> transaction { get; set; }
        public virtual DbSet<transactionDetail> transactionDetail { get; set; }
        public virtual DbSet<employee> employee { get; set; }
        public virtual DbSet<jobTitle> jobTitle { get; set; }
        public virtual DbSet<laborDetail> laborDetail { get; set; }
        public virtual DbSet<posEmployee> posEmployee { get; set; }
        public virtual DbSet<userInfo> userInfo { get; set; }
        public virtual DbSet<userSecurity> userSecurity { get; set; }
        public virtual DbSet<salesDetail> salesDetail { get; set; }
        public virtual DbSet<salesEmployee> salesEmployee { get; set; }
        public virtual DbSet<salesPayment> salesPayment { get; set; }

        protected void OnModelCreatingInt(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<glAccount>()
                .Property(e => e.locationNo)
                .IsUnicode(false);

            modelBuilder.Entity<glAccount>()
                .Property(e => e.legalEntityNumber)
                .IsUnicode(false);

            modelBuilder.Entity<glAccount>()
                .Property(e => e.attribute1Number)
                .IsUnicode(false);

            modelBuilder.Entity<glAccount>()
                .Property(e => e.attribute2Number)
                .IsUnicode(false);

            modelBuilder.Entity<glAccount>()
                .Property(e => e.glAccountNumber)
                .IsUnicode(false);

            modelBuilder.Entity<location>()
                .Property(e => e.locationNo)
                .IsUnicode(false);

            modelBuilder.Entity<location>()
                .Property(e => e.legalEntityNumber)
                .IsUnicode(false);

            modelBuilder.Entity<location>()
                .Property(e => e.attribute1Number)
                .IsUnicode(false);

            modelBuilder.Entity<location>()
                .Property(e => e.attribute2Number)
                .IsUnicode(false);

            modelBuilder.Entity<transaction>()
                .Property(e => e.budgetActual)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<transactionDetail>()
                .Property(e => e.credit)
                .HasPrecision(19, 4);

            modelBuilder.Entity<transactionDetail>()
                .Property(e => e.debit)
                .HasPrecision(19, 4);

            modelBuilder.Entity<transactionDetail>()
                .Property(e => e.amount)
                .HasPrecision(19, 4);

            modelBuilder.Entity<transactionDetail>()
                .Property(e => e.quantity)
                .HasPrecision(23, 10);

            modelBuilder.Entity<transactionDetail>()
                .Property(e => e.previousCountTotal)
                .HasPrecision(19, 4);

            modelBuilder.Entity<transactionDetail>()
                .Property(e => e.adjustment)
                .HasPrecision(19, 4);
        }
    }
}
