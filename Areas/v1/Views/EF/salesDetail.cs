namespace R365.WebApi.Areas.v1.Views.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("salesDetail")]
    public partial class salesDetail
    {
        public Guid? salesDetailId { get; set; }

        [StringLength(100)]
        public string menuitem { get; set; }

        [Column(TypeName = "money")]
        public decimal? Amount { get; set; }

        [StringLength(100)]
        public string CustomerPOSText { get; set; }

        public DateTime? Date { get; set; }

        public decimal? Quantity { get; set; }

        public bool? Void { get; set; }

        public Guid? Company { get; set; }

        public Guid? Location { get; set; }

        public Guid? SalesID { get; set; }

        [StringLength(250)]
        public string SalesAccount { get; set; }

        [StringLength(100)]
        public string category { get; set; }

        public Guid? houseaccounttransaction { get; set; }

        public Guid? TransactionDetailId { get; set; }

        public Guid? CateringEvent { get; set; }
    }
}
