﻿using AutoMapper;
using R365.WebApi.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace R365.WebApi.Areas.v1.Views
{
    public static class AutoMapperConfig
    {

        public static void UseAutoMapper()
        {
            ConfigureAutoMapperForTesting();
        }
        public static void ConfigureAutoMapperForTesting()
        {
            var config = GetMapperConfiguration();
            config.AssertConfigurationIsValid();
        }
        internal static MapperConfiguration GetMapperConfiguration()
        {
            return new MapperConfiguration(config =>
            {
                config.DestinationMemberNamingConvention = new PascalCaseNamingConvention();

                config.CreateMap<EF.transaction, Models.Transaction>()
                    .MapMember(t => t.transactionDate, t => t.Date)
                    .MapMember(t => t.transactionLocationId, t => t.LocationId)
                    .MapMember(t => t.transactionLocationName, t => t.LocationName)
                    .MapMember(t => t.transactionName, t => t.Name)
                    .MapMember(t => t.transactionType, t => t.Type);

                config.CreateMap<EF.company, Models.Company>()
                    .MapMember(c => c.companyName, c => c.Name)
                    .MapMember(c => c.accountId, c => c.CompanyId);

                config.CreateMap<EF.item, Models.Item>()
                    .MapMember(i => i.itemName, i => i.Name)
                    .MapMember(i => i.itemCategory1, i => i.Category1)
                    .MapMember(i => i.itemCategory2, i => i.Category2)
                    .MapMember(i => i.itemCategory3, i => i.Category3);

                config.CreateMap<EF.location, Models.Location>()
                    .MapMember(l => l.locationName, l => l.Name)
                    .MapMember(l => l.locationModifiedOn, l => l.ModifiedOn)
                    .MapMember(l => l.location_Number, l => l.LocationNumber);
                
                config.CreateMap<EF.glAccount, Models.GlAccount>()
                    .MapMember(a => a.glAccountName, a => a.Name)
                    .MapMember(a => a.locationNo, a => a.LocationNumber);
            });
        }
    }
}