﻿using R365.WebApi.Areas.v1.Views.Models;
using R365.WebApi.Authentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.OData;

namespace R365.WebApi.Areas.v1.Views.Controllers
{
    [Authorize]
    [IdentityBasicAuthentication]
    public class PosEmployeeController : ODataControllerBase
    {

        public PosEmployeeController()
            : base()
        {

        }

        [EnableQuery(PageSize = App_Start.ODataConfig.PageSize)]
        public IQueryable<PosEmployee> Get()
        {
            if (!this.CurrentUserId.HasValue)
                return null;

            return this.Repos.PosEmployeeRepo.GetEmployees(this.CurrentUserId.Value);
           
        }

        [EnableQuery]
        public SingleResult<PosEmployee> Get(Guid key)
        {
            if (!this.CurrentUserId.HasValue)
                return null;

            return SingleResult.Create(this.Repos.PosEmployeeRepo.GetEmployees(this.CurrentUserId.Value).Where(a => a.POSEmployeeId == key));
        }
    }
}