﻿using R365.WebApi.Areas.v1.Views.Models;
using R365.WebApi.Authentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.OData;

namespace R365.WebApi.Areas.v1.Views.Controllers
{
    [Authorize]
    [IdentityBasicAuthentication]
    public class SalesEmployeeController : ODataControllerBase
    {

        public SalesEmployeeController()
            : base()
        {

        }

        [EnableQuery(PageSize = App_Start.ODataConfig.PageSize)]
        public IQueryable<SalesEmployee> Get()
        {
            if (!this.CurrentUserId.HasValue)
                return null;

            return this.Repos.SalesEmployeeRepo.GetEmployees(this.CurrentUserId.Value);
           
        }

        [EnableQuery]
        public SingleResult<SalesEmployee> Get(Guid key)
        {
            if (!this.CurrentUserId.HasValue)
                return null;

            return SingleResult.Create(this.Repos.SalesEmployeeRepo.GetEmployees(this.CurrentUserId.Value).Where(a => a.SalesId == key));
        }
    }
}