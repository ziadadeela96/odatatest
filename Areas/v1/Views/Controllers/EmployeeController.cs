﻿using R365.WebApi.Areas.v1.Views.Models;
using R365.WebApi.Authentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.OData;

namespace R365.WebApi.Areas.v1.Views.Controllers
{

    public class EmployeeController : ODataControllerBase
    {

        public EmployeeController()
            : base()
        {

        }

        [EnableQuery(PageSize = App_Start.ODataConfig.PageSize)]
        public IQueryable<Employee> Get()
        {
            if (!this.CurrentUserId.HasValue)
                return null;

            return this.Repos.EmployeeRepo.GetEmployees(this.CurrentUserId.Value);
        }

        [EnableQuery]
        public SingleResult<Employee> Get(Guid key)
        {
            if (!this.CurrentUserId.HasValue)
                return null;

            return SingleResult.Create(this.Repos.EmployeeRepo.GetEmployees(this.CurrentUserId.Value).Where(a => a.EmployeeId == key));
        }
    }
}