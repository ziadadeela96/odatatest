﻿using R365.WebApi.Areas.v1.Views.Models;
using R365.WebApi.Authentication;
using System;
using System.Linq;
using System.Web.Http;
using System.Web.OData;

namespace R365.WebApi.Areas.v1.Views.Controllers
{
    [Authorize]
    [IdentityBasicAuthentication]
    public class SalesDetailController : ODataControllerBase
    {
        public SalesDetailController()
            : base()
        {

        }

        [EnableQuery(PageSize = App_Start.ODataConfig.PageSize)]
        public IQueryable<SalesDetail> Get()
        {
            if (!this.CurrentUserId.HasValue)
                return null;

            return this.Repos.SalesDetailRepo.GetSalesDetails(this.CurrentUserId.Value);
        }

        [EnableQuery]
        public IHttpActionResult Get(Guid key)
        {
            if (!this.CurrentUserId.HasValue)
                return null;

            return Ok(this.Repos.SalesDetailRepo.GetSalesDetails(this.CurrentUserId.Value).Where(salesDetail => salesDetail.salesdetailID == key));
        }
    }
}