﻿using R365.WebApi.Areas.v1.Views.Models;
using R365.WebApi.Authentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.OData;

namespace R365.WebApi.Areas.v1.Views.Controllers
{
    [Authorize]
    [IdentityBasicAuthentication]
    public class ItemController : ODataControllerBase
    {

        public ItemController()
            : base()
        {

        }

        [EnableQuery(PageSize = App_Start.ODataConfig.PageSize)]
        public IQueryable<Item> Get()
        {
            return this.Repos.ItemRepo.GetItems();
        }

        [EnableQuery]
        public SingleResult<Item> Get(Guid key)
        {
            return SingleResult.Create(this.Repos.ItemRepo.GetItems().Where(i => i.ItemId == key));
        }
    }
}