﻿using R365.WebApi.Areas.v1.Views.Models;
using R365.WebApi.Authentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.OData;

namespace R365.WebApi.Areas.v1.Views.Controllers
{
    public class LocationController : ODataControllerBase
    {
        public LocationController()
            : base()
        {

        }

        [EnableQuery(PageSize = App_Start.ODataConfig.PageSize)]
        public IQueryable<Location> Get()
        {
            if (!this.CurrentUserId.HasValue)
                return null;

            return this.Repos.LocationRepo.GetLocations(this.CurrentUserId.Value);
        }

        [EnableQuery]
        public SingleResult<Location> Get(Guid key)
        {
            if (!this.CurrentUserId.HasValue)
                return null;

            return SingleResult.Create(this.Repos.LocationRepo.GetLocations(this.CurrentUserId.Value).Where(l => l.LocationId == key));
        }
    }
}