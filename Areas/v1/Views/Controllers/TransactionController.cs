﻿using R365.WebApi.Areas.v1.Views.Models;
using R365.WebApi.Authentication;
using System;
using System.Data;
using System.Linq;
using System.Web.Http;
using System.Web.OData;
using System.Web.OData.Routing;

namespace R365.WebApi.Areas.v1.Views.Controllers
{
    public class TransactionController : ODataControllerBase
    {
        public TransactionController()
            : base()
        {

        }

        [EnableQuery(PageSize = App_Start.ODataConfig.PageSize)]
        public IQueryable<Transaction> Get()
        {
            if (!this.CurrentUserId.HasValue)
                return null;

            return this.Repos.TransactionRepo.GetTransactions(this.CurrentUserId.Value);
        }

        [EnableQuery]
        public SingleResult<Transaction> Get(Guid key)
        {
            if (!this.CurrentUserId.HasValue)
                return null;

            return SingleResult.Create(this.Repos.TransactionRepo.GetTransactions(this.CurrentUserId.Value).Where(transaction => transaction.TransactionId == key));
        }
    }
}
