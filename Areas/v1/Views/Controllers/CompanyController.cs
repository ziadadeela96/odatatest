﻿using R365.WebApi.Areas.v1.Views.Models;
using R365.WebApi.Authentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.OData;

namespace R365.WebApi.Areas.v1.Views.Controllers
{
    //[Authorize]
    //[IdentityBasicAuthentication]
    public class CompanyController : ODataControllerBase
    {

        public CompanyController()
            : base()
        {

        }

        [EnableQuery()]
        //[EnableQuery(PageSize = App_Start.ODataConfig.PageSize)]
        public IQueryable<Company> Get()
        {
            return this.Repos.CompanyRepo.GetCompanies();
        }

        [EnableQuery]
        public SingleResult<Company> Get(Guid id)
        {
            return SingleResult.Create(this.Repos.CompanyRepo.GetCompanies().Where(c => c.CompanyId == id));
        }

    }
}