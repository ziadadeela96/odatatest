﻿using AutoMapper;
using R365.WebApi.Areas.v1.Views.EF;
using R365.WebApi.Authentication;
using R365.WebApi.Utils;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.OData;

namespace R365.WebApi.Areas.v1.Views.Controllers
{

    public abstract class ODataControllerBase : ODataController
    {
        protected Repositories.Repositories Repos => _repos.Value;
        private Lazy<Repositories.Repositories> _repos;
        private Lazy<ApiV1Context> _context;
        private IMapper _mapper;

        public ODataControllerBase()
        {
            this._context = new Lazy<ApiV1Context>();

            this._repos = new Lazy<Repositories.Repositories>(() => new Repositories.Repositories(this._context.Value));
            this._mapper = AutoMapperConfig.GetMapperConfiguration().CreateMapper();
        }
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (this._context.IsValueCreated)
                this._context.Value.Dispose();
        }
        protected Guid? CurrentUserId
        {
            get { return new Guid("00000000-0000-0000-0000-000000000001"); }
        }
        protected UserIdentity CurrentUser
        {
            get => (UserIdentity)HttpContext.Current?.User?.Identity;
        }
    }
}