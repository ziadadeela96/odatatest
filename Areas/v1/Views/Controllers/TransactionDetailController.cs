﻿using R365.WebApi.Areas.v1.Views.Models;
using R365.WebApi.Authentication;
using System;
using System.Linq;
using System.Web.Http;
using System.Web.OData;

namespace R365.WebApi.Areas.v1.Views.Controllers
{
    public class TransactionDetailController : ODataControllerBase
    {
        public TransactionDetailController()
            : base()
        {

        }

        [EnableQuery(PageSize = App_Start.ODataConfig.PageSize)]
        public IQueryable<TransactionDetail> Get()
        {
            if (!this.CurrentUserId.HasValue)
                return null;

            return this.Repos.TransactionDetailRepo.GetTransactionDetails(this.CurrentUserId.Value);
        }

        [EnableQuery]
        public IHttpActionResult Get(int key)
        {
            if (!this.CurrentUserId.HasValue)
                return null;

            return Ok(this.Repos.TransactionDetailRepo.GetTransactionDetails(this.CurrentUserId.Value).Where(transactionDetail => transactionDetail.TransactionDetailAutoId == key));
        }
    }
}