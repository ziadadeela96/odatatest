﻿using R365.WebApi.Areas.v1.Views.Models;
using R365.WebApi.Authentication;
using System;
using System.Linq;
using System.Web.Http;
using System.Web.OData;

namespace R365.WebApi.Areas.v1.Views.Controllers
{
    [Authorize]
    [IdentityBasicAuthentication]
    public class SalesPaymentController : ODataControllerBase
    {
        public SalesPaymentController()
            : base()
        {

        }

        [EnableQuery(PageSize = App_Start.ODataConfig.PageSize)]
        public IQueryable<SalesPayment> Get()
        {
            if (!this.CurrentUserId.HasValue)
                return null;

            return this.Repos.SalesPaymentRepo.GetSalesPayments(this.CurrentUserId.Value);
        }

        [EnableQuery]
        public IHttpActionResult Get(Guid key)
        {
            if (!this.CurrentUserId.HasValue)
                return null;

            return Ok(this.Repos.SalesPaymentRepo.GetSalesPayments(this.CurrentUserId.Value).Where(salesPayment => salesPayment.salespaymentId == key));
        }
    }
}