﻿using R365.WebApi.Areas.v1.Views.Models;
using R365.WebApi.Authentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.OData;

namespace R365.WebApi.Areas.v1.Views.Controllers
{
    [Authorize]
    [IdentityBasicAuthentication]
    public class JobTitleController : ODataControllerBase
    {

        public JobTitleController()
            : base()
        {

        }

        [EnableQuery(PageSize = App_Start.ODataConfig.PageSize)]
        public IQueryable<JobTitle> Get()
        {
            if (!this.CurrentUserId.HasValue)
                return null;

            return this.Repos.JobTitleRepo.GetJobTitles(this.CurrentUserId.Value);
        }

        [EnableQuery]
        public SingleResult<JobTitle> Get(Guid key)
        {
            if (!this.CurrentUserId.HasValue)
                return null;

            return SingleResult.Create(this.Repos.JobTitleRepo.GetJobTitles(this.CurrentUserId.Value).Where(a => a.JobTitleId == key));
        }
    }
}