﻿using R365.WebApi.Areas.v1.Views.Models;
using R365.WebApi.Authentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.OData;

namespace R365.WebApi.Areas.v1.Views.Controllers
{
    [Authorize]
    [IdentityBasicAuthentication]
    public class GlAccountController : ODataControllerBase
    {

        public GlAccountController()
            : base()
        {

        }

        [EnableQuery(PageSize = App_Start.ODataConfig.PageSize)]
        public IQueryable<GlAccount> Get()
        {
            if (!this.CurrentUserId.HasValue)
                return null;

            return this.Repos.GlAccountRepo.GetGlAccounts(this.CurrentUserId.Value);
        }

        [EnableQuery]
        public SingleResult<GlAccount> Get(int key)
        {
            if (!this.CurrentUserId.HasValue)
                return null;

            return SingleResult.Create(this.Repos.GlAccountRepo.GetGlAccounts(this.CurrentUserId.Value).Where(a => a.GlAccountAutoId == key));
        }
    }
}