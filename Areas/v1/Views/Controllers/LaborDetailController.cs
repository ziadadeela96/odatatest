﻿using R365.WebApi.Areas.v1.Views.Models;
using R365.WebApi.Authentication;
using System;
using System.Linq;
using System.Web.Http;
using System.Web.OData;

namespace R365.WebApi.Areas.v1.Views.Controllers
{
    [Authorize]
    [IdentityBasicAuthentication]
    public class LaborDetailController : ODataControllerBase
    {
        public LaborDetailController()
            : base()
        {

        }

        [EnableQuery(PageSize = App_Start.ODataConfig.PageSize)]
        public IQueryable<LaborDetail> Get()
        {
            if (!this.CurrentUserId.HasValue)
                return null;

            var response =  this.Repos.LaborDetailRepo.GetLaborDetails(this.CurrentUserId.Value);
            return response;
        }

        [EnableQuery]
        public IHttpActionResult Get(Guid key)
        {
            if (!this.CurrentUserId.HasValue)
                return null;

            return Ok(this.Repos.LaborDetailRepo.GetLaborDetails(this.CurrentUserId.Value).Where(laborDetail => laborDetail.laborId == key));
        }
    }
}