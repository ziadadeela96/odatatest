﻿using R365.WebApi.Areas.v1.Views.EF;
using R365.WebApi.Authentication;
using R365.WebApi.EF.R365;
using R365.WebApi.EF.R365Admin;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace R365.WebApi.Utils
{
    public static class CustomerDbHelper
    {
        private static string dbUsername = WebConfigurationManager.AppSettings["DbUsername"];
        private static string dbPassword = WebConfigurationManager.AppSettings["DbPassword"];
        private static string azureDbServerName = WebConfigurationManager.AppSettings["AzureDbServerName"];

        private static Dictionary<string, string> _customerApiDbNames = new Dictionary<string, string>();
        private static Dictionary<string, string> _ipAddresses = new Dictionary<string, string>();
        private static Dictionary<string, string> _apiIpAddresses = new Dictionary<string, string>();

        private static string GetCustomerDbName()
        {
            var host = HttpContext.Current.Request.Url.Host;
            if (host.IndexOf(".") >= 0)
                return host.Substring(0, host.IndexOf("."));
            return host;
        }
        public static string GetCustomerApiDbName(string dbName)
        {
            if (!_customerApiDbNames.ContainsKey(dbName) || string.IsNullOrWhiteSpace(_customerApiDbNames[dbName]))
            {
                using (var adminContext = new R365AdminContext(dbUsername, dbPassword))
                {
                    var customerVersion = adminContext.CustomerVersion.FirstOrDefault(c => c.CustomerInstance == dbName);
                    if (_customerApiDbNames.ContainsKey(dbName))
                        _customerApiDbNames[dbName] = customerVersion?.APIDatabase;
                    else
                        _customerApiDbNames.Add(dbName, customerVersion?.APIDatabase);
                }
            }
            return _customerApiDbNames[dbName];
        }
        private static string GetCustomerIpAddress(string dbName)
        {
            if (!_ipAddresses.ContainsKey(dbName))
            {
                using (var adminContext = new R365AdminContext(dbUsername, dbPassword))
                {
                    var customerVersion = adminContext.CustomerVersion.FirstOrDefault(c => c.CustomerInstance == dbName);
                    _ipAddresses.Add(dbName, customerVersion?.SQLServer);
                }
            }
            return _ipAddresses[dbName];
        }
        private static string GetCustomerApiIpAddress(string dbName)
        {
            if (!_apiIpAddresses.ContainsKey(dbName))
            {
                using (var adminContext = new R365AdminContext(dbUsername, dbPassword))
                {
                    var customerVersion = adminContext.CustomerVersion.FirstOrDefault(c => c.CustomerInstance == dbName);
                    _apiIpAddresses.Add(dbName, customerVersion?.APIServer);
                }
            }
            return _apiIpAddresses[dbName];
        }
        public static ApiV1Context GetApiDbContext(string customerDbName)
        {
            var dbName = GetCustomerApiDbName(customerDbName);
            //var ipAddress = GetCustomerApiIpAddress(customerDbName);

            return new ApiV1Context(azureDbServerName, dbName);
        }
        public static R365Context GetCustomerDbContext(string customerDbName)
        {
            return  new R365Context();
            var ipAddress = GetCustomerIpAddress(customerDbName);
            return new R365Context(ipAddress, customerDbName, dbUsername, dbPassword);
        }
    }
}