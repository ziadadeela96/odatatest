﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace R365.WebApi.Utils
{
    public static class AutoMapperHelper
    {
        public static IMappingExpression<T, T1> MapMember<T, T1, TSourceMember, TDestMember>(this IMappingExpression<T, T1> expression, Expression<Func<T, TSourceMember>> sourceMember, Expression<Func<T1, TDestMember>> destinationMember)
        {
            return expression.ForMember(destinationMember, opts => opts.MapFrom(sourceMember));
        }
    }
}