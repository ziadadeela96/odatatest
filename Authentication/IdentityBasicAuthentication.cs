﻿using R365.WebApi.Utils;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Filters;

namespace R365.WebApi.Authentication
{
    public class IdentityBasicAuthentication : Attribute, IAuthenticationFilter
    {
        public bool AllowMultiple { get { return false; } }
        private static List<string> _validApiRoleNames = new List<string>() {
            "Accounting Clerk",
            "Accounting Manager",
            "Business Analytics Admin",
            "Business Analytics View Only", // Business Analytics Admin View Only
        };

        public async Task AuthenticateAsync(HttpAuthenticationContext context, CancellationToken cancellationToken)
        {
            var credentials = BasicAuthenticationHelper.ExtractCredentialsFromAuthorizationHeader(context);
            if (string.IsNullOrWhiteSpace(credentials?.Domain))
                return;

            var principal = await AuthenticateAsync(credentials.Domain, credentials.UserName, credentials.Password);
            if (principal == null)
                context.ErrorResult = new AuthenticationFailureResult("Invalid username or password", context.Request);
            else
                context.Principal = principal;

        }
#pragma warning disable CS1998
        public async Task ChallengeAsync(HttpAuthenticationChallengeContext context, CancellationToken cancellationToken)
        {
            var challenge = new AuthenticationHeaderValue("Basic");
            context.Result = new AddChallengeOnUnauthorizedResult(challenge, context.Result);
        }
#pragma warning restore  CS1998
        private async Task<IPrincipal> AuthenticateAsync(string domain, string userName, string password)
        {

            var context = CustomerDbHelper.GetCustomerDbContext(domain);
            //var user = await (from u in context.SystemUserBase
            //                  where u.dm_Login == userName && u.dm_LoginPassword == password && u.IsDisabled != true
            //                  select u).FirstOrDefaultAsync();

            var user = context.SystemUserBase.First();

            var hasAppropriatePermissions = false;
            if (user != null)
            {
                var userRoles = await (from r in context.Roles
                                       join ur in context.UserRole on r.RoleId equals ur.RoleId
                                       where ur.UserId == user.SystemUserId
                                       select r).ToListAsync();

                hasAppropriatePermissions = user.FullAccess == true || user.dm_R365 == true || userRoles.Any(r => _validApiRoleNames.Contains(r.Name));
            }

            // See if the DB has been setup
            var dbIsSetup = false;
            if (user != null)
                //dbIsSetup = !string.IsNullOrWhiteSpace(CustomerDbHelper.GetCustomerApiDbName(domain));
                dbIsSetup = true;

            var userPrincipal = new UserPrincipal()
            {
                Identity = new UserIdentity()
                {
                    UserId = user?.SystemUserId,
                    Domain = domain,
                    AuthenticationType = "Basic",
                    IsAuthenticated = user != null && dbIsSetup && hasAppropriatePermissions,
                    Name = user?.FullName,
                    IsGlobalAdmin = user?.dm_R365 ?? false,
                    CanViewAllLocations = user?.dm_AvailableToAllLocations ?? false
                }
            };
            return userPrincipal;
        }
    }

    public class AuthenticationFailureResult : IHttpActionResult
    {
        public AuthenticationFailureResult(string reasonPhrase, HttpRequestMessage request)
        {
            ReasonPhrase = reasonPhrase;
            Request = request;
        }

        public string ReasonPhrase { get; private set; }

        public HttpRequestMessage Request { get; private set; }

        public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            return Task.FromResult(Execute());
        }

        private HttpResponseMessage Execute()
        {
            var response = new HttpResponseMessage(HttpStatusCode.Unauthorized);
            response.RequestMessage = Request;
            response.ReasonPhrase = ReasonPhrase;
            return response;
        }
    }
    public class AddChallengeOnUnauthorizedResult : IHttpActionResult
    {
        public AddChallengeOnUnauthorizedResult(AuthenticationHeaderValue challenge, IHttpActionResult innerResult)
        {
            Challenge = challenge;
            InnerResult = innerResult;
        }

        public AuthenticationHeaderValue Challenge { get; private set; }

        public IHttpActionResult InnerResult { get; private set; }

        public async Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            var response = await InnerResult.ExecuteAsync(cancellationToken);
            if (response.StatusCode == HttpStatusCode.Unauthorized)
            {
                // Only add one challenge per authentication scheme.
                if (!response.Headers.WwwAuthenticate.Any((h) => h.Scheme == Challenge.Scheme))
                {
                    response.Headers.WwwAuthenticate.Add(Challenge);
                }
            }

            return response;
        }
    }

    public class UserPrincipal : IPrincipal
    {
        public IIdentity Identity { get; internal set; }

        public bool IsInRole(string role)
        {
            return false;
        }
    }
    public class UserIdentity : IIdentity
    {
        public string Name { get; internal set; }

        public string AuthenticationType { get; internal set; }

        public bool IsAuthenticated { get; internal set; }

        public Guid? UserId { get; internal set; }
        public string Domain { get; internal set; }
        public bool IsGlobalAdmin { get; internal set; }
        public bool CanViewAllLocations { get; internal set; }
    }
}