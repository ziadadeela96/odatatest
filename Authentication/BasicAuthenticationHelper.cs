﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Http.Filters;

namespace R365.WebApi.Authentication
{
    public static class BasicAuthenticationHelper
    {
        public static BasicCredentials ExtractCredentialsFromAuthorizationHeader(HttpAuthenticationContext context)
        {
            var request = context.Request;
            var authorization = request.Headers.Authorization;

            if (authorization == null || authorization.Scheme != "Basic")
                return null;

            // 4. If there are credentials that the filter understands, try to validate them.
            // 5. If the credentials are bad, set the error result.
            if (String.IsNullOrEmpty(authorization.Parameter))
            {
                context.ErrorResult = new AuthenticationFailureResult("Invalid credentials", context.Request);
                return null;
            }

            var encoding = Encoding.GetEncoding("iso-8859-1");
            var usernamePassword = encoding.GetString(Convert.FromBase64String(authorization.Parameter));
            int seperatorIndex = usernamePassword.IndexOf(':');

            var userName = usernamePassword.Substring(0, seperatorIndex);
            var password = usernamePassword.Substring(seperatorIndex + 1);
            var domain = string.Empty;

            if (userName.Contains("\\") || userName.Contains("/"))
            {
                var indexOf = userName.IndexOf("\\");
                if (indexOf < 0)
                    indexOf = userName.IndexOf("/");

                domain = userName.Substring(0, indexOf);
                userName = userName.Substring(indexOf + 1);
            }

            return new BasicCredentials()
            {
                UserName = userName,
                Password = password,
                Domain = domain
            };
        }
    }
    public class BasicCredentials
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Domain { get; set; }
    }
}